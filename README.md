## Requirements:
You should go through these steps to use application. All steps are required except those that are marked as optional.
1. Download or clone project from `https://gitlab.com/eykhmvic/eClubAsseco`. Let's call installed folder MAIN.
2. Be sure that you install following libraries in python
    1. scipy
    2. graphviz
    3. jupyter
    4. matplotlib
    5. numpy
    6. pandas
    7. pymongo
    8. sacred
    9. scikit-learn
3. Set environment variable PYTHON_EXE
4. Set environment variable PYTHONPATH to `MAIN/pythonModule/pythonDir` 
5. Put data in `MAIN/pythonModule/pythonDir/manufacture_command_ex` folder. For example, "hudba_manufacture_command_ex.csv"
6. Put data in `MAIN/pythonModule/pythonDir/data_cont` folder. For example, "pribory_data_cont.csv"




## Running the project from command line

Run `mvn clean install spring-boot:run` in the project root directory. After the server has started point your browser to [http://localhost:8080](http://localhost:8080) to see the resulting application.

## Running the project from your IDE

Navigate to the `Main` class and run it as a Java application.
