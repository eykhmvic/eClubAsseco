package org.kopi.plotly.data.types;

import com.vaadin.shared.ui.colorpicker.Color;
import org.kopi.plotly.data.dataseries.AbstractDataSeries;
import org.kopi.plotly.data.dataseries.CoupleOfData;
import org.kopi.plotly.data.dataseries.SingleData;
import org.kopi.plotly.data.features.Colors;
import org.kopi.plotly.data.features.modeData;
import org.kopi.plotly.data.line.Line;
import org.kopi.plotly.data.marker.BarMarker;

import javax.json.JsonArray;

public class HistogramData extends AbstractData {

    private TypeData type;

    @Override
    public TypeData getType() {
        return type;
    }

    public HistogramData(String name) {
        super(name, new SingleData());
        type = TypeData.HISTOGRAM;
    }

    public HistogramData() {
        super(new SingleData());
        type = TypeData.HISTOGRAM;
    }

    @Override
    public String getColor() {
        return "red";
    }
    @Override
    public String getOrientation() {
        return "ver";
    }



}
