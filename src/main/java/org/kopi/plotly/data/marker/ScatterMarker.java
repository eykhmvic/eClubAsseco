package org.kopi.plotly.data.marker;


import org.kopi.plotly.data.features.Colors;

import com.vaadin.shared.ui.colorpicker.Color;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author sbchini
 */

public class ScatterMarker extends AbstractMarker {

    //---------------------------------------------------------
    // CONSTRUCTOR
    //---------------------------------------------------------

    public ScatterMarker() {
//    this.color = Colors.LIGHTSKYBLUE;
        color = new ArrayList<>();
        color.add(Colors.LIGHTSKYBLUE);
        color.add(Colors.LIGHTGREEN);
        color.add(Colors.LIGHTSALMON);
        color.add(Colors.LIGHTCORAL);
        color.add(Colors.LIGHTSTEELBLUE);
        color.add(Colors.LIGHTGRAY);
        this.opacity = 0.8;
        this.size = 10;
    }

    //---------------------------------------------------------
    // IMPLEMENTATIONS
    //---------------------------------------------------------

    /**
     * Sets the marker color.
     * @param color
     */

//    public void setColor(Color color) {
//        this.color = color;
//    }

    /**
     * Sets the marker size.
     *
     * @param
     */
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    protected int getSize() {
        return size;
    }

    public void setColor(List<Color> color) {
        this.color = color;
    }

    @Override
    public Collection<Color> getColors() {
        return color;
    }

    @Override
    public double getOpacity() {
        return opacity;
    }

//    @Override
//    public String getColor() {
//        return null;
////        return "rgba(" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + ",0.8)";
//    }


    //---------------------------------------------------------
    // DATA MEMBERS
    //---------------------------------------------------------

    private int size;
    private double opacity;
//    private Color color;
    private List<Color> color;


}
