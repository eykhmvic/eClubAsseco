package my.asseco.api;

import lombok.Data;

import java.util.HashMap;
@Data
public class Classifier {
    public String name;
    public HashMap<String, Double> results;
}
