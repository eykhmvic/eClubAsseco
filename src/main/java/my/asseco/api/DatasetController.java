package my.asseco.api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import my.asseco.Tools.ClassifierManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class DatasetController {

        private static final String API = "api";

    @RequestMapping(value = "/dataset",
            method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<List<Dataset>> findDataset() {
        Dataset dataset1 = new Dataset();
        dataset1.setName("name1");
        Dataset dataset2 = new Dataset();
        dataset2.setName("name2");
        List<Dataset> list = new ArrayList<>();
        list.add(dataset1);
        list.add(dataset2);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @RequestMapping(value = API + "/classifier",
            method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<List<Classifier>> findClassifier() {
        ArrayList<Classifier> list = new ArrayList<>();
        ClassifierManager classifierManager = new ClassifierManager();
        for (String name : classifierManager.getClassifiersName()) {
            Classifier classifier = new Classifier();
            classifier.setName(name);
            classifier.setResults(classifierManager.getHashMapResultsForClassifier().get(name));
            list.add(classifier);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }


}
