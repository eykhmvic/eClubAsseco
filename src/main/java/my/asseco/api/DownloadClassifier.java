package my.asseco.api;


import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import static my.asseco.Tools.Constants.CLASSIFIER_FOLDER;

@RestController
public class DownloadClassifier {
    private final static String API = "/api";


    @SneakyThrows
    @RequestMapping(value = "/download/{file_name}", method = RequestMethod.GET)
    public void getFile(@PathVariable("file_name") String fileName, HttpServletResponse response) {
        InputStream is = new FileInputStream(new File(CLASSIFIER_FOLDER + fileName + ".clsid"));
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + ".clsid" + "\"");
        org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();

    }
}
