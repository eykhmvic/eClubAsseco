package my.asseco.Tools;


import java.io.File;

public class Constants {

    final static public String getPythonEXE() {
        return System.getenv("PYTHON_EXE");
    }


    final static public String PYTHON_DIR = "pythonModule" + File.separator + "pythonDir" + File.separator ;
    //    final static public String PYTHON_EXE = "C:"+File.separator+"Users"+File.separator+"VEykhmann"+File.separator+"Downloads"+File.separator+"forPython"+File.separator+"Scripts"+File.separator+"python.exe";
    final static public String[] COLUMNS_FOR_CLASSIFIER = new String[]{"command_price", "start_month", "nrow_TAC", "sum_TAC", "material_sum", "material_nrow", "parent_pieces_order_count", "parent_pieces_order_mean", "parent_pieces_order_max", "parent_pieces_order_min", "parent_depth", "parent_count", "end_stock_price", "actual_cost", "ongoing_same_pieces", "ongoing_same_command", "ongoing_all_command", "ongoing_all_pieces"};
    final static public String ECLUB_ASSECO_FOLDER = "src" + File.separator + "main" + File.separator + "java" + File.separator + "my" + File.separator + "asseco" + File.separator + "eclubasseco" + File.separator;
    final static public String DATASET_FOLDER = "pythonModule" + File.separator + "pythonDir" + File.separator + "manufacture_command_ex" + File.separator;
    final static public String TIME_SERIES_FOLDER = "pythonModule" + File.separator + "pythonDir" + File.separator + "data_cont" + File.separator;
    final static public String SERIALIZABLE_FOLDER_MANUFACTURE = "src" + File.separator + "main" + File.separator + "java" + File.separator + "my" + File.separator + "asseco" + File.separator + "SerializableObjects" + File.separator + "manufacture_command_ex" + File.separator;
    final static public String SERIALIZABLE_FOLDER_TIMESERIES = "src" + File.separator + "main" + File.separator + "java" + File.separator + "my" + File.separator + "asseco" + File.separator + "SerializableObjects" + File.separator + "data_cont" + File.separator;
    final static public String CLASSIFIER_FOLDER = "pythonModule" + File.separator + "pythonDir" + File.separator + "classifiers" + File.separator ;
    final static public boolean isTest = true;

}
