package my.asseco.Tools;

import com.vaadin.spring.annotation.SpringComponent;
import lombok.SneakyThrows;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.PrintWriter;

import static my.asseco.Tools.Constants.*;

@SpringComponent
public class Initializer {

    private static void deleteEmptyFile(File file) {
        for (File f : (file.listFiles())) {
            if (f.toString().equals(file + "\\.empty")) {
                f.delete();
                return;
            }
        }
    }

    @SneakyThrows
    private static void createEmptyFile(File file) {
        PrintWriter writer = new PrintWriter(file + "\\.empty", "UTF-8");
        writer.println("empty");
        writer.close();
    }

    private static void delete() {
        deleteEmptyFile(new File(PYTHON_DIR + "gen_cls"));
        deleteEmptyFile(new File(DATASET_FOLDER));
        deleteEmptyFile(new File(CLASSIFIER_FOLDER));
        deleteEmptyFile(new File(TIME_SERIES_FOLDER));
        deleteEmptyFile(new File(SERIALIZABLE_FOLDER_MANUFACTURE));
        deleteEmptyFile(new File(SERIALIZABLE_FOLDER_TIMESERIES));
    }

    private static void create() {
        createEmptyFile(new File(PYTHON_DIR + "gen_cls"));
        createEmptyFile(new File(DATASET_FOLDER));
        createEmptyFile(new File(CLASSIFIER_FOLDER));
        createEmptyFile(new File(TIME_SERIES_FOLDER));
        createEmptyFile(new File(SERIALIZABLE_FOLDER_MANUFACTURE));
        createEmptyFile(new File(SERIALIZABLE_FOLDER_TIMESERIES));
    }

    @PostConstruct
    public void init(){
        delete();
//        create();
        GraphManager.main(null);
        TimeSeriesManager.main(null);
    }
}
