package my.asseco.Tools;

import com.vaadin.spring.annotation.SpringComponent;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.util.HashMap;
import java.util.List;

import static my.asseco.Tools.Constants.COLUMNS_FOR_CLASSIFIER;
import static my.asseco.Tools.Constants.PYTHON_DIR;
import static my.asseco.Tools.Constants.getPythonEXE;


@SpringComponent
public class PythonCaller {
    /*
     * size is needed to test if incoming parameters contains right size
     */
    private int size = COLUMNS_FOR_CLASSIFIER.length;

    private HashMap<String, BufferedReader> hashMapBufferedReaders = new HashMap<>();
    private HashMap<String, OutputStream> hashMapBufferedWriters = new HashMap<>();

    @Autowired
    private ClassifierManager classifierManager;


    /**
     * @param nameOfClassifier which will evaluate
     * @param values           that will be evaluated according to COLUMNS_FOR_CLASSIFIER
     * @return String from python script
     */
    @SneakyThrows
    public String evaluate(String nameOfClassifier, List<String> values) {
        if (nameOfClassifier == null) {
            return null;
        }
        System.out.println("Evaluate with nameOfClassifier: " + nameOfClassifier);
        if (!hashMapBufferedReaders.containsKey(nameOfClassifier)) {
            String[] cmd = {getPythonEXE(),
                    PYTHON_DIR + "mypython.py", "--classifier", nameOfClassifier + ".clsid"};
            Process p = Runtime.getRuntime().exec(cmd);
            hashMapBufferedReaders.put(nameOfClassifier, new BufferedReader(new
                    InputStreamReader(p.getInputStream())));
            hashMapBufferedWriters.put(nameOfClassifier, p.getOutputStream());
            hashMapBufferedReaders.get(nameOfClassifier).readLine();
        }
        BufferedReader stdInput = hashMapBufferedReaders.get(nameOfClassifier);
        OutputStream stdOutput = hashMapBufferedWriters.get(nameOfClassifier);

        try {
            if (values.size() != size) {
                System.out.println("Wrong size. Expected: " + size + " got: " + values.size());
                return null;
            }
            for (String s : values) {
                stdOutput.write((s + "\n").getBytes());
            }
            stdOutput.flush();
            return stdInput.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param parameters for classifier -str -etr etc..
     * @return parameters in specific structure that will be passed to own_classifier.py
     */
    @SneakyThrows
    public String getGenerageClassifier(String... parameters) {
        String[] param = {getPythonEXE(),
                "experiments" + File.separator + "generate_classifier_description.py"};

        ProcessBuilder pb = new ProcessBuilder();
        pb.command(ArrayUtils.addAll(param, parameters));
        pb.directory(new File(PYTHON_DIR));
        Process p = pb.start();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        while ((line = errorReader.readLine()) != null) {
            System.err.println(line);
        }
        return bufferedReader.readLine();
    }

    /**
     * Create new classifier. Add classifier to folder, and send classifier results to classifier manager
     * Run script own_classifier with parameters
     *
     * @param paramsForOwnClassifier needed for own_classifier.py
     * @param name                   needed for own_classifier.py
     * @param id                     needed for own_classifier.py
     */
    @SneakyThrows
    public void runOwnClassifier(String paramsForOwnClassifier, String name, String id) {
        System.out.println(paramsForOwnClassifier);
        HashMap<String, Double> hashMap = new HashMap<>();
        ProcessBuilder processBuilder = new ProcessBuilder(getPythonEXE(),
                "own_classifier.py", paramsForOwnClassifier.replace("\\\"","\""), name, id);
        processBuilder.directory(new File(PYTHON_DIR));
        Process process = processBuilder.start();
        BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(process.getInputStream()));
        BufferedReader eror = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        String line;
        while ((line = bufferedReader2.readLine()) != null) {
            String[] splits = (line.split(" "));
            switch (splits[0]) {
                case "[]_exact_acc[[(_penalty,gt,1{_}00M)]]":
                    hashMap.put("RED", Double.valueOf(splits[2]));
                    break;
                case "[]_exact_acc[[(_penalty,le,250{_}00k)]]":
                    hashMap.put("GREEN", Double.valueOf(splits[2]));
                    break;
                case "[]_exact_acc[[(_penalty,gt,250{_}00k)&(_penalty,le,1{_}00M)]]":
                    hashMap.put("YELLOW", Double.valueOf(splits[2]));
                    break;
                case "[]_exact_acc":
                    hashMap.put("GENERAL", Double.valueOf(splits[2]));
                    break;
            }
        }
        if(hashMap.containsKey("RED")&&hashMap.containsKey("GREEN")&&hashMap.containsKey("YELLOW")){
            hashMap.put("geom_mean",getGeomMean(hashMap.get("RED"), hashMap.get("GREEN"), hashMap.get("YELLOW")));
        }
        while ((line=eror.readLine())!=null){
            System.err.println(line);
        }
        classifierManager.addResults(hashMap, name);
    }

    private double getGeomMean(double r, double g, double y) {
        return Math.cbrt(r * g * y);
    }
    public static String[] example = {"-c", "pribory",
            "-str", "2014",
            "-etr", "2016",
            "-ste", "2016",
            "-ete", "2017"};


}
