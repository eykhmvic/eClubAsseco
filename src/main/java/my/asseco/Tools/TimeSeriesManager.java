package my.asseco.Tools;


import com.vaadin.spring.annotation.SpringComponent;
import lombok.SneakyThrows;
import my.asseco.Model.TimeSeries;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static my.asseco.Tools.Constants.*;

@SpringComponent
public class TimeSeriesManager {

    private static HashMap<String, TimeSeries> timeSeriesHashMap = new HashMap<>();

    /**
     * Convert CSV file to TimeSeries graph
     *
     * @param dataset name of file. For example, if dataset="hudba", file hudba_data_cont.csv will be readed
     * @return TimeSeries graph
     * @throws IOException while reading file
     */
    private static TimeSeries readFromFile(String dataset) throws IOException {
        TimeSeries timeSeries = new TimeSeries();
        List<String> dates;
        HashMap<String, List<String>> hashMap;
        List<List<String>> lines = new ArrayList<>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(TIME_SERIES_FOLDER + dataset + ".csv"));
        String line;
        int i = 0, max = 200;
        while ((line = bufferedReader.readLine()) != null) {
            if (i++ > max && isTest) break;
            List<String> splits = Arrays.asList(line.split(","));
            lines.add(splits);
        }
        dates = lines.get(0)
                .subList(2, lines.get(0).size())
                .stream()
                .map(o -> convert(Double.valueOf(o).longValue()))
                .collect(Collectors.toList());
        hashMap = new HashMap<>();
        lines.subList(1, lines.size()).forEach(o -> hashMap.put(o.get(0), o.subList(2, o.size())));
        timeSeries.setDates(dates);
        timeSeries.setHashMap(hashMap);
        return timeSeries;
    }

    /**
     * Convert time from UNIX format
     *
     * @param l time in UNIX format
     * @return String in form yyyy-MM-dd
     */
    private static String convert(long l) {
        Date date = new Date(l * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    /**
     * Serialize all timeSeries graphs. Run with parameters at least -Xmx3g
     */
    @SneakyThrows
    private static void serializeAll() {
        TimeSeries timeSeries;
        for (String name : getTimeSeriesNames()) {
            timeSeries = readFromFile(name);
            timeSeries.serialize(name);
            timeSeries.setDataset(name);
        }
    }


    public static TimeSeries getTS(String name) {
        return timeSeriesHashMap.get(name);
    }


    /**
     * @return first TimeSeries. Needed as default value
     */
    public static TimeSeries getFirst() {
        String name = timeSeriesHashMap.keySet().iterator().next();
        return timeSeriesHashMap.get(name);
    }

    /**
     * @return list of all timeSeries names in TIME_SERIES_FOLDER
     */
    public static ArrayList<String> getTimeSeriesNames() {
        ArrayList<String> datasetNames = new ArrayList<>();
        for (File f : (new File(TIME_SERIES_FOLDER).listFiles())) {
            if (f.getName().equals(".empty")) continue;
            datasetNames.add(f.getName().substring(0, f.getName().indexOf(".")));
        }
        return datasetNames;
    }


    @PostConstruct
    private void deserializeAll() {
        ArrayList<String> datasetNames = getTimeSeriesNames();
        for (String s : datasetNames) {
            timeSeriesHashMap.put(s, TimeSeries.deserialize(s));
        }
    }

    public boolean test() {
        try {
            serializeAll();
            deserializeAll();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private static boolean allserialized() {
        File scvFiles = new File(TIME_SERIES_FOLDER);
        ArrayList<File> jsonFiles = new ArrayList<File>(Arrays.asList((new File(SERIALIZABLE_FOLDER_TIMESERIES).listFiles())));
        ArrayList<String> jsonNames = new ArrayList<>();
        for (File f : jsonFiles) {
            jsonNames.add(f.getName().substring(0, f.getName().indexOf(".")));
        }
        for (File f : (scvFiles.listFiles())) {
            String name = f.getName().substring(0, f.getName().indexOf("."));
            if (!jsonNames.contains(name)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isEmptyTimeSeries = false;

    public static void main(String[] args) {
        TimeSeriesManager timeSeries = new TimeSeriesManager();
        isEmptyTimeSeries = getTimeSeriesNames().size() <= 0;
        serializeAll();
        timeSeries.deserializeAll();
        if (isEmptyTimeSeries) return;
        if (!timeSeries.allserialized()) {
            timeSeries.serializeAll();
            timeSeries.deserializeAll();
            System.out.println("Done OK");
        } else {
            System.out.println("Already OK");
        }
    }

    @SneakyThrows
    public static void refresh() {
        serializeAll();
        isEmptyTimeSeries = getTimeSeriesNames().size() <= 0;
//        System.out.println(getTimeSeriesNames);
        TimeSeries current;
        for (String name : getTimeSeriesNames()) {
            if (!timeSeriesHashMap.containsKey(name)) {
                current = readFromFile(name);
                timeSeriesHashMap.put(name, TimeSeries.deserialize(name));
            }
        }


    }
}
