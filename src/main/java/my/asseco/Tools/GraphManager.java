package my.asseco.Tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.vaadin.spring.annotation.SpringComponent;
import lombok.Data;
import lombok.SneakyThrows;
import my.asseco.Model.Graph;

import javax.annotation.PostConstruct;
import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static my.asseco.Tools.Constants.*;

/**
 * Class that manage graphs
 */
@SpringComponent
@Data
public class GraphManager {
    public static String[] nameOfAttributes;
    public static HashMap<String, List<String>> myHashMap;
    public static HashMap<String, Graph> graphs = new HashMap<>();

    public static HashMap<String, List<String>> getMyHashMap() {
        return myHashMap;
    }

    public static Graph getGraph(String name) {
        return graphs.get(name);
    }

    /**
     * Read file with manufacture_command_ex.csv structure.
     *
     * @param dataset is first part of the file name. For example if dataset = "hudba", method read hudba_manufacture_command_ex.csv file
     * @return Splitted rows
     */
    @SneakyThrows
    private static ArrayList<String[]> readFile(String dataset) {
        ArrayList<String[]> items = new ArrayList<>();
        List<String> lines = Files.readAllLines(Paths.get(DATASET_FOLDER + dataset + ".csv"));
        String cvsSplitBy = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
        nameOfAttributes = lines.get(0).split(cvsSplitBy);
        for (int i = 1; i < lines.size(); i++) {
            String[] strings = lines.get(i).split(cvsSplitBy);
            items.add(strings);
        }
        return items;
    }


    /**
     * Serialize description_asseco_min.csv file, that contains description, units, into json file myFile
     */
    @SneakyThrows
    private static void serializeMy() {
        List<String> lines = Files.readAllLines(Paths.get(ECLUB_ASSECO_FOLDER + "description_asseco_min.csv"));
        String cvsSplitBy = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
        myHashMap = new HashMap<>();
        String[] firstRow = lines.get(0).split(cvsSplitBy);
        for (String s : firstRow) {
            myHashMap.put(s, new ArrayList<>());
        }
        for (int i = 1; i < lines.size(); i++) {
            String[] strings = lines.get(i).split(cvsSplitBy);
            myHashMap.get("name").add(strings[0]);
            myHashMap.get("units").add(strings[1]);
            myHashMap.get("description_cz").add(strings[2]);
            myHashMap.get("description_en").add(strings[3]);
            myHashMap.get("normal_name").add(strings[4]);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File(SERIALIZABLE_FOLDER_MANUFACTURE + "myFile" + ".json"), myHashMap);
    }


    /**
     * Serialize all graphs from CSV files to JSON files. Serialize names lf attributes
     */
    @SneakyThrows
    public static void serializeAll() {
        refreshDatasetNames();
        Graph current;
        for (String name : datasetNames) {
            current = new Graph();
            current.setItems(GraphManager.readFile(name));
            current.serialize(name);
        }
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(SERIALIZABLE_FOLDER_MANUFACTURE + "attributes" + ".json"), nameOfAttributes);
        serializeMy();
    }

    public static void refreshGraphs() {
        refreshDatasetNames();
        serializeMy();
        serializeAll();
        emptyDatasets = datasetNames.size() <= 0;
        System.out.println(datasetNames);
        Graph current;
        for (String name : datasetNames) {
            if (!graphs.containsKey(name)) {
                current = new Graph();
                current.setItems(GraphManager.readFile(name));
                current.serialize(name);
                graphs.put(name, Graph.deserialize(name));
            }
        }

    }


    /**
     * @return first graph's name in HashMap, needed as default value
     */
    public static Graph getFirstGraph() {
        String firstName = graphs.keySet().iterator().next();
        return graphs.get(firstName);
    }

    public static ArrayList<String> datasetNames;

    public static ArrayList<String> getDatasetNames() {
        return datasetNames;
    }

    /**
     * refresh datasets names
     */
    public static void refreshDatasetNames() {
        ArrayList<String> currentTimeSeriesNames = new ArrayList<>();
        for (File f : (new File(DATASET_FOLDER).listFiles())) {
            if (f.getName().equals(".empty")) continue;
            currentTimeSeriesNames.add(f.getName().substring(0, f.getName().indexOf(".")));
        }
        datasetNames = currentTimeSeriesNames;
    }

    /**
     * Deserialize all graphs that are in SERIALIZABLE_FOLDER in JSON format.
     * Deserialize attributes
     * Deserialize myFile with units, description
     */
    @PostConstruct
    @SneakyThrows
    private static void deserializeAll() {
        refreshDatasetNames();
        for (String s : datasetNames) {
            graphs.put(s, Graph.deserialize(s));
        }
        ObjectMapper mapper = new ObjectMapper();
        if (Files.exists(Paths.get(SERIALIZABLE_FOLDER_MANUFACTURE + "attributes.json")))
            nameOfAttributes = mapper.readValue(new File(SERIALIZABLE_FOLDER_MANUFACTURE + "attributes" + ".json"), String[].class);

        ObjectReader forHashMap = new ObjectMapper().readerFor(HashMap.class);
        if (Files.exists(Paths.get(SERIALIZABLE_FOLDER_MANUFACTURE + "myFile.json")))
            myHashMap = forHashMap.readValue(new File(SERIALIZABLE_FOLDER_MANUFACTURE + "myFile" + ".json"));
    }

    /**
     * For testing serializable graphs
     *
     * @return true if there is no exceptions, else return false
     */
    public boolean test() {
        try {
            serializeMy();
            serializeAll();
            deserializeAll();
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    private static boolean allserialized() {
        File scvFiles = new File(DATASET_FOLDER);
        ArrayList<File> jsonFiles = new ArrayList<File>(Arrays.asList((new File(SERIALIZABLE_FOLDER_MANUFACTURE).listFiles())));
        ArrayList<String> jsonNames = new ArrayList<>();
        for (File f : jsonFiles) {
            jsonNames.add(f.getName().substring(0, f.getName().indexOf(".")));
        }
        for (File f : (scvFiles.listFiles())) {
            String name = f.getName().substring(0, f.getName().indexOf("."));
            if (!jsonNames.contains(name)) {
                return false;
            }
        }
        return true;
    }

    public static boolean emptyDatasets = false;

    public static void main(String[] args) {
        refreshDatasetNames();
        emptyDatasets = datasetNames.size() <= 0;
        serializeMy();
        serializeAll();
        deserializeAll();
        System.out.println("Done OK");
    }
}
