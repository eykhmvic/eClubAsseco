package my.asseco.Tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

/**
 * Manage classifiers
 */
@Data
@Component
public class ClassifierManager {

    ArrayList<String> classifiersName = new ArrayList<>();
    File classifiersFolder = new File(Constants.CLASSIFIER_FOLDER);
    File serializesResultsFile = new File(Constants.CLASSIFIER_FOLDER + "results.json");
    HashMap<String, HashMap<String, Double>> hashMapResultsForClassifier = new HashMap<>();

    /**
     * Read classifiers results from classifierFolder/results.json to HashMap. Keys in hashMap means name of classifier
     * Read classifiers from classifierFolder into ArrayList classifiersName
     */
    @SneakyThrows
    @PostConstruct
    public void init() {
        ObjectMapper mapper = new ObjectMapper();
        if (Files.readAllLines(Paths.get(serializesResultsFile.toString())).size() != 0) {
            hashMapResultsForClassifier = mapper.readValue(serializesResultsFile, hashMapResultsForClassifier.getClass());
        }
        for (File f : (classifiersFolder.listFiles())) {
            if (!f.getName().contains("tree") && f.getName().contains(".cls"))
                classifiersName.add(f.getName().substring(0, f.getName().indexOf(".")));
        }
        for (String cName : classifiersName) {
            if (!hashMapResultsForClassifier.containsKey(cName)) {
                hashMapResultsForClassifier.put(cName, new HashMap<>());
            }
        }
    }

    /**
     * Add new classifier's name and classifier's result into ArrayList and into HashMap.
     * Write classifier into result.json file.
     *
     * @param hashMap results of classiifer
     * @param name    name of classifier
     */
    @SneakyThrows
    public void addResults(HashMap<String, Double> hashMap, String name) {
        classifiersName.add(name);
        this.hashMapResultsForClassifier.put(name, hashMap);
        new ObjectMapper().writeValue(serializesResultsFile, this.hashMapResultsForClassifier);
    }


}
