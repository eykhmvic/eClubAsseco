package my.asseco;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;


@SpringUI
@SpringViewDisplay
public class MyUI extends UI {
    @Override
    protected void init(VaadinRequest request) {
        getPage().setTitle("eClubAsseco");
    }


}
