package my.asseco.Model;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.FormLayout;
import lombok.Data;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static my.asseco.Tools.Constants.COLUMNS_FOR_CLASSIFIER;

@Data
@SpringComponent
public class ClassifierParameters extends FormLayout {
    public HashMap<String, Integer> idClassifiersHashMap = new HashMap<>();

    @PostConstruct
    public void init() {
        for (int i = 0; i < classifierList.length; i++) {
            idClassifiersHashMap.put(classifierList[i], i);
        }
    }

    public void setNumber(String name) {
        this.number = idClassifiersHashMap.get(name).toString();
    }


    String[] classifierList = new String[]{"KNeighborsClassifier(1)",
            "KNeighborsClassifier(2)",
            "KNeighborsClassifier(3)",
            "KNeighborsClassifier(4)",
            "KNeighborsClassifier(5)",
            "KNeighborsClassifier(6)",
            "KNeighborsClassifier(7)",
            "SVC(kernel=\"linear\", C=0.025)",
            "SVC(gamma=2, C=1)",
            "DecisionTreeClassifier(max_depth=5)",
            "DecisionTreeClassifier(max_depth=6)",
            "DecisionTreeClassifier(max_depth=7)",
            "DecisionTreeClassifier(max_depth=8)",
            "RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1)",
            "RandomForestClassifier(max_depth=5, n_estimators=15, max_features=1)",
            "RandomForestClassifier(max_depth=5, n_estimators=20, max_features=1)",
            "RandomForestClassifier(max_depth=8, n_estimators=10, max_features=1)",
            "RandomForestClassifier(max_depth=8, n_estimators=15, max_features=1)",
            "RandomForestClassifier(max_depth=8, n_estimators=20, max_features=1)",
            "MLPClassifier(alpha=1)",
            "AdaBoostClassifier()",
            "GaussianNB()",
            "QuadraticDiscriminantAnalysis()"};


    public String number = "0";
    List<String> columns = Arrays.asList(COLUMNS_FOR_CLASSIFIER);
    public String penalty = "cost_sin_2_6";
    public String company="";
    public String start_years_train = "2014";
    public String start_years_test = "2016";
    public String end_years_train = "2016";
    public String end_years_test = "2017";
    public String pbs_job_name = "my_pbs_job_name";
    public String pbs_jobid = "id";
    public String uniform;
    public String log;
    public String count;

    public String[] getAll() {
        return
                new String[]
                        {
                                "-n", number,
                                "-p", penalty,
                                "-c", company,
                                "-str", start_years_train,
                                "-ste", start_years_test,
                                "-etr", end_years_train,
                                "-ete", end_years_test,
                                "-pjn", pbs_job_name,
                                "-pjd", pbs_jobid
                        };
    }

}
