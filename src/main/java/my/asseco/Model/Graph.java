package my.asseco.Model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.SneakyThrows;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import static my.asseco.Tools.GraphManager.myHashMap;
import static my.asseco.Tools.GraphManager.nameOfAttributes;
import static my.asseco.Tools.Constants.*;

@Data
public class Graph implements Serializable {

    ArrayList<String[]> items;

    @SneakyThrows
    public void serialize(String name) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(SERIALIZABLE_FOLDER_MANUFACTURE + name + ".json"), this);
    }

    @SneakyThrows
    public static Graph deserialize(String dataset) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(SERIALIZABLE_FOLDER_MANUFACTURE + dataset + ".json"), Graph.class);
    }

    public List<String> getColumn(String columnName) {
        ArrayList<String> list = new ArrayList<>();
        int indexOfColumnName = getIndexOfColumn(columnName);
        for (String[] item : items) {
            list.add(item[indexOfColumnName]);
        }
        return list;
    }

    public List<String> getDateColumn(String columnName) {
        ArrayList<String> list = new ArrayList<>();
        for (String[] item : items) {
            list.add(convert(Double.valueOf(item[getIndexOfColumn(columnName)]).longValue()));
        }
        return list;
    }

    private static String convert(long l) {
        Date date = new java.util.Date(l * 1000L);
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static int getIndexOfColumn(String columnName) {
        int indexColumnNameInMyHashMap = myHashMap.get("normal_name").indexOf(columnName);

        if (indexColumnNameInMyHashMap == -1) {
            for (int i = 0; i < nameOfAttributes.length; i++) {
                if (nameOfAttributes[i].equals(columnName)) {
                    return i;
                }
            }
        }

        String notNormalName = myHashMap.get("name").get(indexColumnNameInMyHashMap);

        for (int i = 0; i < nameOfAttributes.length; i++) {
            if (nameOfAttributes[i].equals(notNormalName)) {
                return i;
            }
        }

        return 0;
    }

    public String[] getRow(int index) {
        return items.get(index);
    }

}
