package my.asseco.Model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.SneakyThrows;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import static my.asseco.Tools.Constants.SERIALIZABLE_FOLDER_TIMESERIES;

@Data
public class TimeSeries {
    String dataset;
    List<String> dates;
    HashMap<String, List<String>> hashMap;

    @SneakyThrows
    public static TimeSeries deserialize(String name) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(SERIALIZABLE_FOLDER_TIMESERIES + name + ".json"), TimeSeries.class);
    }

    @SneakyThrows
    public  void serialize(String name) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(new File(SERIALIZABLE_FOLDER_TIMESERIES + name + ".json"), this);
    }

}
