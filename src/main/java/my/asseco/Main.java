package my.asseco;

import lombok.SneakyThrows;
import my.asseco.Tools.GraphManager;
import my.asseco.Tools.Initializer;
import my.asseco.Tools.TimeSeriesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.PrintWriter;
import java.util.Objects;

import static my.asseco.Tools.Constants.*;

@SpringBootApplication
public class Main {

    @Autowired
    Initializer initializer;
    public static void main(String[] args) {

        SpringApplication.run(Main.class, args);
    }
}
