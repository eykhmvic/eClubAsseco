package my.asseco.View;

import com.vaadin.server.Page;
import com.vaadin.ui.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import my.asseco.Tools.GraphManager;
import my.asseco.Tools.TimeSeriesManager;
import org.springframework.context.annotation.Scope;

import java.io.FileOutputStream;
import java.io.OutputStream;

import static my.asseco.Tools.Constants.DATASET_FOLDER;
import static my.asseco.Tools.Constants.ECLUB_ASSECO_FOLDER;
import static my.asseco.Tools.Constants.TIME_SERIES_FOLDER;

/**
 * Modal window for adding new dataset
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Scope("prototype")
public class MySubWindowForAddingDataset extends Window {

    Button close;
    MyReceiver receiver = new MyReceiver();
    Upload upload = new Upload("Upload it here", receiver);

    boolean dataset;

    /**
     * Init vew, add listeners, download logic
     *
     * @param close button for closing view. Add as parameter, because close can have listeners outside of this scope
     */
    MySubWindowForAddingDataset(Button close, boolean dataset) {
        this.close = close;
        this.dataset = dataset;
        setClosable(true);
        setCaption("Add new dataset");
        center();
        setModal(true);
        addCloseShortcut(27);
        setContent(init());
    }

    private VerticalLayout init() {
        VerticalLayout verticalLayout = new VerticalLayout();
        //upload
        upload.setImmediateMode(false);
        upload.setButtonCaption("Import");
        upload.addSucceededListener(receiver);
        upload.addFinishedListener(receiver);
        upload.addStartedListener(receiver);
        verticalLayout.addComponent(upload);
        verticalLayout.addComponent(close);
        close.addClickListener(e -> close());

        return verticalLayout;
    }


    /**
     * Class responsible for downloading
     */
    class MyReceiver implements Upload.Receiver, Upload.SucceededListener, Upload.FinishedListener, Upload.StartedListener {

        @Override
        @SneakyThrows
        public OutputStream receiveUpload(String filename,
                                          String mimeType) {
            if (mimeType.equals("application/vnd.ms-excel")) {
                return new FileOutputStream((dataset ? DATASET_FOLDER : TIME_SERIES_FOLDER) + filename);
            }
            return new FileOutputStream(ECLUB_ASSECO_FOLDER + "trash.txt");
        }

        @Override
        public void uploadSucceeded(Upload.SucceededEvent succeededEvent) {
            Notification notification = new Notification("File uploaded");
            notification.setDelayMsec(1000);
            notification.show(Page.getCurrent());
            if (dataset) {
                GraphManager.refreshGraphs();
            } else {
                TimeSeriesManager.refresh();
            }

        }

        @Override
        public void uploadFinished(Upload.FinishedEvent finishedEvent) {
            close.click();
        }

        @Override
        public void uploadStarted(Upload.StartedEvent startedEvent) {
            String contentType = startedEvent.getMIMEType();
            if (!contentType.equals("application/vnd.ms-excel")) {
                Notification.show("Error", "\nAllowed MIME: " + ".csv\n click me", Notification.Type.ERROR_MESSAGE);
                upload.interruptUpload();
            }
        }
    }


}
