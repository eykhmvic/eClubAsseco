package my.asseco.View;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.annotation.PostConstruct;

/**
 * Navigation bar is on top of each page. Navigate to different pages.
 */
@UIScope
@SpringComponent
public class MyNavigationBar extends VerticalLayout implements View {

    @Autowired
    private Navigator navigator;

    @PostConstruct
    public void init() {
        setMargin(false);

        VerticalLayout layout = new VerticalLayout();
        MenuBar barmenu = new MenuBar();

        layout.setMargin(false);
        layout.setSpacing(false);
        layout.setSizeFull();

        //navigate to graph with scatter, historgram, boxplot, timeSeries graphs
        barmenu.addItem("Graph", null, s -> navigator.navigateTo(""));

        //navigate to page with graph with different classifiers results. Allows to compare several classifiers
        barmenu.addItem("Train Classifier", null, s -> navigator.navigateTo("trainClassifier"));

        //navigate to page, that allows to choose and use classifier. GYR colored
        barmenu.addItem("Use classifier", null, s -> navigator.navigateTo("useClassifier"));

        layout.addComponent(barmenu);

        addComponent(layout);

    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
