package my.asseco.View;

import com.vaadin.shared.ui.colorpicker.Color;
import lombok.Data;
import lombok.SneakyThrows;
import my.asseco.Model.Graph;
import my.asseco.Tools.GraphManager;
import org.kopi.plotly.PlotlyChart;
import org.kopi.plotly.PlotlyFactory;
import org.kopi.plotly.configuration.DataConfiguration;
import org.kopi.plotly.configuration.LayoutConfiguration;
import org.kopi.plotly.data.types.HistogramData;
import org.kopi.plotly.layout.LayoutWithAxis;

@Data
public class HistogramGraph {

    private HistogramData histogramData;
    private LayoutWithAxis layoutWithAxis;
    private String columnName;
    private Graph dataset;

    HistogramGraph(String columnName, Graph dataset) {
        this.columnName = columnName;
        this.dataset = dataset;
    }

    public void changeColumnName(String columnName) {
        this.columnName = columnName;
    }

    public PlotlyChart getGraph() {
        changeLayout();
        changeData();
        DataConfiguration dataConfiguration = new DataConfiguration();
        dataConfiguration.addDataConfiguration(histogramData);
        return PlotlyFactory.renderChart(dataConfiguration, new LayoutConfiguration(layoutWithAxis));
    }


    @SneakyThrows
    private void changeData() {
        HistogramData histogramData = new HistogramData();
        for (String s : dataset.getColumn(columnName)) {
            histogramData.getData().addData(Double.valueOf(s));
        }
        this.histogramData = histogramData;
    }

    private void changeLayout() {
        LayoutWithAxis layout = new LayoutWithAxis();
        layout.getYaxis().setType("log");
        layout.disableLegend();
//        layout.getXaxis().setTitle(columnName);
        layout.getXaxis().setTitle(columnName+ " [" + GraphManager.myHashMap.get("units").get(GraphManager.myHashMap.get("normal_name").indexOf(columnName)) + "]");
        layout.setBackgroundColor(new Color(250, 250, 250));
        this.layoutWithAxis = layout;
    }
}
