package my.asseco.View;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.AlignmentInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import my.asseco.Model.TimeSeries;
import my.asseco.Tools.GraphManager;
import my.asseco.Tools.TimeSeriesManager;

import javax.annotation.PostConstruct;

@UIScope
@SpringComponent
public class TabWithTimeSeries extends VerticalLayout {
    private NativeSelect<String> datasetSelect;

    private void refreshDatasetSelect() {
        datasetSelect.setItems(TimeSeriesManager.getTimeSeriesNames());
    }

    /**
     * Render view
     */
    @PostConstruct
    public void init() {
        if (TimeSeriesManager.isEmptyTimeSeries) {
            Button close = new Button("Close", e -> {
                removeAllComponents();
                init();
            });
            addComponent(new AddTimeSeries(close));
            return;
        }
        //native select for use dataset
        datasetSelect = new NativeSelect<>("Choose dataset");
        datasetSelect.setItems(TimeSeriesManager.getTimeSeriesNames());
        datasetSelect.setValue(TimeSeriesManager.getTimeSeriesNames().get(0));
        datasetSelect.setEmptySelectionAllowed(false);

        Button addNew = new Button("Add");
        addNew.setIcon(VaadinIcons.PLUS);
        addNew.setStyleName(ValoTheme.BUTTON_PRIMARY);
        //close button
        Button close = new Button("Close", event -> refreshDatasetSelect());
        close.setStyleName(ValoTheme.BUTTON_DANGER);

        MySubWindowForAddingDataset mySubWindowForAddingDataset = new MySubWindowForAddingDataset(close, false);
        addNew.addClickListener(clickEvent -> {
            UI.getCurrent().addWindow(mySubWindowForAddingDataset);
            JavaScript.getCurrent().execute("document.getElementsByClassName('gwt-FileUpload')[0].setAttribute('accept', '.csv')");

        });

        HorizontalLayout datasetAndAdd = new HorizontalLayout(datasetSelect, addNew);
        datasetAndAdd.setComponentAlignment(addNew, new Alignment(AlignmentInfo.Bits.ALIGNMENT_BOTTOM));
        datasetAndAdd.setMargin(false);
        TimeSeriesView timeSeriesView = new TimeSeriesView(TimeSeriesManager.getFirst());

        //tab for time series graph
        addComponents(datasetAndAdd, timeSeriesView.getView());


        datasetSelect.addSelectionListener(singleSelectionEvent -> {
            TimeSeries newTimeSeries = TimeSeriesManager.getTS(singleSelectionEvent.getSelectedItem().get());
            timeSeriesView.setDataset(newTimeSeries);
            removeAllComponents();
            addComponents(datasetAndAdd, timeSeriesView.getView());

        });
    }

    private void addListeners() {

    }
}
