package my.asseco.View;

import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import my.asseco.Model.Graph;
import my.asseco.MyUI;
import my.asseco.Tools.ClassifierManager;
import my.asseco.Tools.GraphManager;
import my.asseco.Tools.PythonCaller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static my.asseco.Tools.Constants.COLUMNS_FOR_CLASSIFIER;

@SpringView(name = "useClassifier")
public class UseClassifier extends VerticalLayout implements View {
    @Autowired
    private
    UseClassifierGraph useClassifierGraph;

    @Autowired
    private ClassifierManager classifierManager;

    @Autowired
    private PythonCaller pythonCaller;

    @Autowired
    Navigator navigator;
    private String selectedClassifier;

    @Autowired
    private MyNavigationBar myNavigationBar;
    private Grid<String[]> grid;
    private VerticalLayout history;
    private NativeSelect<String> classifierSelect;
    private Button evaluate;
    private Button forEach;
    private VerticalLayout gridAndButton;
    private VerticalLayout evaluateAndHistory;
    private HorizontalLayout gridAndForm;

    @PostConstruct
    public void init() {
        initJavaScript();
        initHistory();
        initClassifierSelect();
        initEvaluate();
        initForEach();
        initGrid();
        initUseClassifierGraph();
        initGridAndForm();
        initThis();
    }

    private void initGrid() {
        // set grid
        grid = new Grid<>();
        if (GraphManager.getFirstGraph() != null)
            grid.setItems(GraphManager.getFirstGraph().getItems());
        for (String s : COLUMNS_FOR_CLASSIFIER) {
            grid.addColumn(strings -> strings[Graph.getIndexOfColumn(s)]).setWidth(150).setCaption(s);
        }
        grid.asSingleSelect().addValueChangeListener(valueChangeEvent ->
        {
            if (valueChangeEvent.getValue() != null) {

                HashMap<String, Double> hashMap = new HashMap<>();
                for (String col : COLUMNS_FOR_CLASSIFIER) {
                    int index = Graph.getIndexOfColumn(col);
                    hashMap.put(col, Double.valueOf(valueChangeEvent.getValue()[index]));
                }

                useClassifierGraph.setHashMap(hashMap);
            }
        });
        grid.setWidth("100%");
        grid.setHeightByRows(20);
    }

    private void initUseClassifierGraph() {
        useClassifierGraph.setMargin(new MarginInfo(false, true));
        useClassifierGraph.setFocusListener(focusEvent -> {
            grid.deselectAll();
        });
        useClassifierGraph.init();

    }

    private void initClassifierSelect() {
        classifierSelect = new NativeSelect<>("Classifier");
        classifierSelect.setItems(classifierManager.getClassifiersName());
        if (classifierManager.getClassifiersName().size() > 0) {
            classifierSelect.setValue(classifierManager.getClassifiersName().get(0));
            selectedClassifier = classifierSelect.getSelectedItem().get();
        }
        classifierSelect.addSelectionListener(singleSelectionEvent -> {
            selectedClassifier = singleSelectionEvent.getValue();
        });
    }

    private void initHistory() {
        history = new VerticalLayout();
        history.setMargin(false);
    }

    private void initJavaScript() {
        Page.Styles styles = Page.getCurrent().getStyles();
        styles.add(".red { background-color: red; } .yellow {background-color: yellow;} .green {background-color: green;}");
    }

    private void initEvaluate() {
        //evaluate button
        evaluate = new Button("Evaluate");
        evaluate.addClickListener(clickEvent -> {
            HashMap<String, Double> hashMap = useClassifierGraph.getHasMap();
            List<String> toSend = new ArrayList<>();
            for (String s : COLUMNS_FOR_CLASSIFIER) {
                toSend.add(hashMap.get(s).toString());
            }

            String result = pythonCaller.evaluate(selectedClassifier, toSend);
            if (result != null) {
                Label label = new Label("penalty: " + result);
                label.setCaption(selectedClassifier + ": ");
                setStyleName(Integer.valueOf(result), label);
                history.addComponentAsFirst(label);
            } else {
                Label label = new Label("Please select classifier!");
                history.addComponent(label);
            }

        });
    }

    private void initForEach() {
        forEach = new Button("forEach");
        forEach.addClickListener(clickEvent -> {
            for (String aClassifiersName : classifierManager.getClassifiersName()) {
                classifierSelect.setValue(aClassifiersName);
                evaluate.click();
            }
        });
    }

    private void initGridAndForm() {
        gridAndButton = new VerticalLayout(grid, forEach, classifierSelect);
        evaluateAndHistory = new VerticalLayout(evaluate, history);
        gridAndForm = new HorizontalLayout(gridAndButton, useClassifierGraph, evaluateAndHistory);
        gridAndForm.setSizeFull();
        gridAndForm.setExpandRatio(gridAndButton, 9);
        gridAndForm.setExpandRatio(useClassifierGraph, 2);
        gridAndForm.setExpandRatio(evaluateAndHistory, 2);
    }

    private void initThis() {
        setWidth("100%");
        setMargin(false);
        addComponent(myNavigationBar);
        addComponent(gridAndForm);
    }

    private String[] styles = new String[]{"green", "yellow", "red"};

    private void setStyleName(int result, Label label) {
        for (int i = 0; i < styles.length; i++) {
            if (result == i) {
                label.setStyleName(styles[i]);
            } else {
                label.removeStyleName(styles[i]);
            }
        }
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }


}
