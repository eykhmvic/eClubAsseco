package my.asseco.View;

import com.vaadin.data.Binder;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import my.asseco.Model.ClassifierParameters;
import my.asseco.Tools.ClassifierManager;
import my.asseco.Tools.PythonCaller;
import org.kopi.plotly.PlotlyChart;
import org.kopi.plotly.PlotlyFactory;
import org.kopi.plotly.configuration.DataConfiguration;
import org.kopi.plotly.configuration.LayoutConfiguration;
import org.kopi.plotly.data.types.ScatterData;
import org.kopi.plotly.layout.LayoutWithAxis;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
@Data
@SpringView(name = "trainClassifier")
public class TrainClassifier extends VerticalLayout implements View {
    @Autowired
    MyNavigationBar myNavigationBar;
    @Autowired
    PythonCaller pythonCaller;
    @Autowired
    ClassifierParameters classifierParameters;
    @Autowired
    Navigator navigator;
    @Autowired
    ClassifierManager classifierManager;

    Button trainAll;
    NativeSelect<String> number;
    Button train;
    Button show;
    HorizontalLayout horizontal;
    VerticalLayout form;
    VerticalLayout listAndButton;
    Label selectedClassifier;
    static int currentX = 0;
    LayoutWithAxis layoutWithAxis;
    Set<String> selectedNamesOfClassifiers;
    ListSelect<String> classifierList = new ListSelect<>("Classifier");
    PlotlyChart graph;
    DatasetSelect companySelect;
    Binder<ClassifierParameters> binder = new Binder<>(ClassifierParameters.class);
    Link download = new Link("Download", new ExternalResource("http://localhost:8080/download/"));


    /**
     * Init view, add listeners and javaScript
     */
    @PostConstruct
    public void init() {
        setMargin(false);
        horizontal = new HorizontalLayout();
        horizontal.setMargin(false);
        horizontal.setSpacing(false);

        //add navigation bar
        addComponents(myNavigationBar, horizontal);

        Button close = new Button("close");
        companySelect = new DatasetSelect(close);

        classifierParameters.setCompany(companySelect.getValue());
        classifierParameters.setNumber(classifierParameters.getIdClassifiersHashMap().keySet().iterator().next());

        number = new NativeSelect<>("Classifier");
        number.setEmptySelectionAllowed(false);
        number.setItems(classifierParameters.getIdClassifiersHashMap().keySet());
        number.setValue(classifierParameters.getIdClassifiersHashMap().keySet().iterator().next());
        TextField start_years_train = new TextField("Start train years");
        binder.bind(start_years_train, ClassifierParameters::getStart_years_train, ClassifierParameters::setStart_years_train);
        TextField start_years_test = new TextField("Start test years");
        binder.bind(start_years_test, ClassifierParameters::getStart_years_test, ClassifierParameters::setStart_years_test);
        TextField end_years_train = new TextField("End train years");
        binder.bind(end_years_train, ClassifierParameters::getEnd_years_train, ClassifierParameters::setEnd_years_train);
        TextField end_years_test = new TextField("End test years");
        binder.bind(end_years_test, ClassifierParameters::getEnd_years_test, ClassifierParameters::setEnd_years_test);
        TextField pbs_job_name = new TextField("Your name of classifier");
        binder.bind(pbs_job_name, ClassifierParameters::getPbs_job_name, ClassifierParameters::setPbs_job_name);
        binder.setBean(classifierParameters);

        train = new Button("Train");
        trainAll = new Button("Train All");
        form = new VerticalLayout(companySelect, number, start_years_train, start_years_test, end_years_train, end_years_test, pbs_job_name, train, trainAll);
        show = new Button("Show");

        classifierList.setItems(
                classifierManager.getClassifiersName()
        );

        selectedClassifier = new Label("");

        JavaScript.getCurrent().addFunction("setCurrent", (JavaScriptFunction) jsonArray -> {
            selectedClassifier.setValue(jsonArray.getString(0));
            download.setCaption(jsonArray.getString(0));
            download.setResource(new ExternalResource("http://localhost:8080/download/" + jsonArray.getString(0)));
        });


        listAndButton = new VerticalLayout(classifierList, show);
        if (classifierManager.getClassifiersName().size() <= 0) {
            listAndButton.setVisible(false);
        }
        horizontal.addComponents(form, listAndButton);
        addListeners();
    }


    // data for plotly chart
    private DataConfiguration getDataConfiguration(Set<String> namesOfClassifers) {
        layoutWithAxis = new LayoutWithAxis();
        layoutWithAxis.disableLegend();
        layoutWithAxis.setWidth(800);
        layoutWithAxis.setHeight(500);
        layoutWithAxis.setyAxisTitle("Accuracy [%]");
        layoutWithAxis.setxAxisTitle("Classifiers");
        List<ScatterData> scatterDataList = new ArrayList<>();
        currentX = 0;


        String nameOfMax = classifierManager.getHashMapResultsForClassifier().keySet().iterator().next();
        double maxGeomMean = classifierManager.getHashMapResultsForClassifier().get(nameOfMax).getOrDefault("geom_mean", Double.MIN_VALUE);
        for (String s : namesOfClassifers) {

            if (classifierManager.getHashMapResultsForClassifier().get(s).containsKey("geom_mean")) {
                double currentGeomMean = classifierManager.getHashMapResultsForClassifier().get(s).get("geom_mean");
                if (maxGeomMean < currentGeomMean) {
                    maxGeomMean = currentGeomMean;
                    nameOfMax = s;
                }


            }
            scatterDataList.add(getScatterDataFromHashMap(classifierManager.getHashMapResultsForClassifier().get(s), s, 10));
        }
        scatterDataList.add(getScatterDataFromHashMap(classifierManager.getHashMapResultsForClassifier().get(nameOfMax), nameOfMax, 15));
        DataConfiguration dataConfiguration = new DataConfiguration();
        for (ScatterData s : scatterDataList) {
            dataConfiguration.addDataConfiguration(s);
        }
        return dataConfiguration;
    }

    /**
     * Add classifier results to graph
     *
     * @param hashMap with resutls GREEN, YELLOW, RED, GENERAL
     * @param name    of classifier
     * @return data for plotting graph in plotly
     */
    @SneakyThrows
    public ScatterData getScatterDataFromHashMap(HashMap<String, Double> hashMap, String name, int size) {
        ScatterData scatterData = new ScatterData();
        int x = getCurrentX();

        scatterData.getData().addData(x, hashMap.containsKey("GREEN") ? hashMap.get("GREEN") : 0);
        scatterData.getData().addData(x, hashMap.containsKey("YELLOW") ? hashMap.get("YELLOW") : 0);
        scatterData.getData().addData(x, hashMap.containsKey("RED") ? hashMap.get("RED") : 0);
        scatterData.getData().addData(x, hashMap.containsKey("GENERAL") ? hashMap.get("GENERAL") : 0);
        scatterData.setColor(Color.RED);
        List<Color> colorList = new ArrayList<>();
        colorList.add(Color.GREEN);
        colorList.add(Color.YELLOW);
        colorList.add(Color.RED);
        colorList.add(Color.BLACK);
        scatterData.setColors(colorList);
        scatterData.getMarker().setSize(size);
        scatterData.setHoverinfo("y+name");
        scatterData.setName(name);
        return scatterData;
    }

    private void addListeners() {
        companySelect.addSelectionListener(singleSelectionEvent -> {
            classifierParameters.setCompany(singleSelectionEvent.getSelectedItem().get());
        });
        trainAll.addClickListener(clickEvent -> {
            for (String s : classifierParameters.getIdClassifiersHashMap().keySet()) {
                number.setSelectedItem(s);
                classifierParameters.setNumber(s);
                train.click();
            }
        });
        number.addValueChangeListener(v->classifierParameters.setNumber(v.getValue()));
        train.addClickListener(clickEvent -> {
            String parametersForOwnClassifier = pythonCaller.getGenerageClassifier(binder.getBean().getAll());
            if (parametersForOwnClassifier == null) {
                Notification.show("Can not call experiments/generate_classifier_description.py\n");
                return;
            }
            String generateName = binder.getBean().getPbs_job_name() + "_" + binder.getBean().getIdClassifiersHashMap().get(number.getSelectedItem().get()) + "_" + binder.getBean().getStart_years_train();
            pythonCaller.runOwnClassifier(parametersForOwnClassifier, generateName, binder.getBean().getPbs_jobid());
            classifierList.setItems(classifierManager.getClassifiersName());
            if (classifierManager.getClassifiersName().size() > 0) {
                listAndButton.setVisible(true);
            }
            classifierList.select(classifierManager.getClassifiersName().toArray(new String[classifierManager.getClassifiersName().size()]));
            show.click();
        });
        classifierList.addSelectionListener(multiSelectionEvent -> {
            selectedNamesOfClassifiers = multiSelectionEvent.getAllSelectedItems();
        });
        show.addClickListener(clickEvent -> {
            horizontal.removeAllComponents();
            graph = PlotlyFactory.renderChart(getDataConfiguration(selectedNamesOfClassifiers), new LayoutConfiguration(layoutWithAxis));
            Page.getCurrent().getJavaScript().execute(graph.getDomId() + ".on('plotly_click', function(data){\n" +
                    "    var pts = '';\n" +
                    "    for(var i=0; i < data.points.length; i++){\n" +
                    "        pts = ''+data.points[i].data.name" +
                    "    }\n" +
                    "    setCurrent(pts);\n" +
                    "});");

            horizontal.addComponents(form, listAndButton, new VerticalLayout(graph, selectedClassifier, download));
        });

    }

    //used for plotting scatter on graphs. For first classifier x will be 0, for second 1 and so on.
    private static int getCurrentX() {
        return currentX++;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
