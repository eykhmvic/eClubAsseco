package my.asseco.View;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.JavaScript;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

public class AddTimeSeries extends HorizontalLayout {

    AddTimeSeries(Button close) {
        Button addNew = new Button("Add");
        addNew.setIcon(VaadinIcons.PLUS);
        addNew.setStyleName(ValoTheme.BUTTON_PRIMARY);
        //close button

        close.setStyleName(ValoTheme.BUTTON_DANGER);

        MySubWindowForAddingDataset mySubWindowForAddingDataset = new MySubWindowForAddingDataset(close,false);
        addNew.addClickListener(clickEvent -> {
            UI.getCurrent().addWindow(mySubWindowForAddingDataset);
            JavaScript.getCurrent().execute("document.getElementsByClassName('gwt-FileUpload')[0].setAttribute('accept', '.csv')");

        });

        addComponent(addNew);


    }




}
