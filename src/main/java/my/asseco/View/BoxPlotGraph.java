package my.asseco.View;

import com.vaadin.shared.ui.colorpicker.Color;
import lombok.Data;
import lombok.SneakyThrows;
import my.asseco.Model.Graph;
import my.asseco.Tools.GraphManager;
import org.kopi.plotly.PlotlyChart;
import org.kopi.plotly.PlotlyFactory;
import org.kopi.plotly.configuration.DataConfiguration;
import org.kopi.plotly.configuration.LayoutConfiguration;
import org.kopi.plotly.data.features.Colors;
import org.kopi.plotly.data.types.BoxPlotData;
import org.kopi.plotly.layout.LayoutWithAxis;

@Data
public class BoxPlotGraph {

    private String s1, s2;
    private PlotlyChart plotlyChart;
    private BoxPlotData boxPlotDataX;
    private BoxPlotData boxPlotDataY;
    private LayoutWithAxis layout;
    private Graph dataset;

    BoxPlotGraph(String s1, String s2, Graph dataset) {
        this.s1 = s1;
        this.s2 = s2;
        this.dataset = dataset;
        changeX(s1);
        changeY(s2);
        changeLayout();
    }


    @SneakyThrows
    public void changeX(String s1) {
        this.s1 = s1;
        BoxPlotData box1 = new BoxPlotData(s1 + " [" + GraphManager.myHashMap.get("units").get(GraphManager.myHashMap.get("normal_name").indexOf(s1)) + "]");
        for (String s : dataset.getColumn(s1)) {
            box1.getData().addData(Double.valueOf(s));
        }
        box1.setColor(Colors.OLIVE);
        this.boxPlotDataX = box1;
    }

    @SneakyThrows
    public void changeY(String s2) {
        this.s2 = s2;
        BoxPlotData box2 = new BoxPlotData(s2 + " [" + GraphManager.myHashMap.get("units").get(GraphManager.myHashMap.get("normal_name").indexOf(s2)) + "]");
        for (String s : dataset.getColumn(s2)) {
            box2.getData().addData(Double.valueOf(s));
        }
        box2.setColor(Colors.BLACK);
        this.boxPlotDataY = box2;
    }


    public PlotlyChart getGraph() {
        DataConfiguration dataBox = new DataConfiguration();
        dataBox.addDataConfiguration(boxPlotDataX);
        dataBox.addDataConfiguration(boxPlotDataY);

        LayoutConfiguration boxBox = new LayoutConfiguration(layout);
        this.plotlyChart = PlotlyFactory.renderChart(dataBox, boxBox);
        return plotlyChart;
    }

    private void changeLayout() {
        LayoutWithAxis layout = new LayoutWithAxis();
        layout.setBackgroundColor(new Color(250, 250, 250));
        layout.getYaxis().setType("log");
        this.layout = layout;

    }

}
