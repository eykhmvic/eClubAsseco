package my.asseco.View;

import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.shared.ui.AlignmentInfo;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import my.asseco.Tools.GraphManager;

public class DatasetSelect extends HorizontalLayout {
    private NativeSelect<String> datasetSelect = new NativeSelect<>();

    //init view, add listeners and javaScript
    DatasetSelect(Button close) {
        datasetSelect.setCaption("Choose dataset");
        if (GraphManager.getDatasetNames() != null && GraphManager.getDatasetNames().size() != 0) {
            //native select for use dataset
            datasetSelect.setItems(GraphManager.getDatasetNames());
            datasetSelect.setValue(GraphManager.getDatasetNames().iterator().next());
            datasetSelect.setEmptySelectionAllowed(false);
        }

        Button addNew = new Button("Add");
        addNew.setIcon(VaadinIcons.PLUS);
        addNew.setStyleName(ValoTheme.BUTTON_PRIMARY);
        //close button
//        Button close = new Button("Close", event -> refreshDatasetSelect());
        close.addClickListener(event -> refreshDatasetSelect());
        close.setStyleName(ValoTheme.BUTTON_DANGER);

        MySubWindowForAddingDataset mySubWindowForAddingDataset = new MySubWindowForAddingDataset(close, true);
        addNew.addClickListener(clickEvent -> {
            UI.getCurrent().addWindow(mySubWindowForAddingDataset);
            JavaScript.getCurrent().execute("document.getElementsByClassName('gwt-FileUpload')[0].setAttribute('accept', '.csv')");

        });

        addComponents(datasetSelect, addNew);
        setComponentAlignment(addNew, new Alignment(AlignmentInfo.Bits.ALIGNMENT_BOTTOM));
        setMargin(false);


    }

    private void refreshDatasetSelect() {
        datasetSelect.setItems(GraphManager.getDatasetNames());
    }

    public void addSelectionListener(SingleSelectionListener<String> listener) {
        datasetSelect.addSelectionListener(listener);
    }

    public String getValue() {
        return datasetSelect.getValue();
    }
}
