package my.asseco.View;

import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.spring.annotation.UIScope;
import lombok.SneakyThrows;
import my.asseco.Model.Graph;
import my.asseco.Tools.GraphManager;
import org.kopi.plotly.PlotlyChart;
import org.kopi.plotly.PlotlyFactory;
import org.kopi.plotly.configuration.DataConfiguration;
import org.kopi.plotly.configuration.LayoutConfiguration;
import org.kopi.plotly.data.types.ScatterData;
import org.kopi.plotly.layout.LayoutWithAxis;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Scatter graph represents dependency one column on another. Colored by column colorColumnName. For example, cost_sin_2_6_60
 */
@UIScope
public class ScatterGraph {
    private String xName, yName, colorColumnName = "cost_penalty_60";
    private boolean xLog = true, yLog = true;
    private LayoutWithAxis layout;
    private ScatterData scatterData;
    private PlotlyChart plotlyChart;
    private Graph dataset;

    ScatterGraph(String xName, String yName, Graph dataset) {
        this.xName = xName;
        this.yName = yName;
        this.dataset = dataset;
    }

    /**
     * Render data and layout
     *
     * @return return graph
     */
    public PlotlyChart getGraph() {
        getScattersLayout();
        changeScatterDate();
        DataConfiguration dataConfiguration = new DataConfiguration();
        dataConfiguration.addDataConfiguration(scatterData);

        this.plotlyChart = PlotlyFactory.renderChart(dataConfiguration, new LayoutConfiguration(layout));
        return plotlyChart;
    }

    /**
     * Change x axis
     *
     * @param xName for x axis
     */
    public void changeX(String xName) {
        this.xName = xName;
    }

    /**
     * Change y axis
     *
     * @param yName for y axis
     */
    public void changeY(String yName) {
        this.yName = yName;
    }

    /**
     * change scale
     *
     * @param xLog if xlog is true scale is logarithmic, otherwise linear
     */
    public void xLog(boolean xLog) {
        this.xLog = xLog;
    }

    /**
     * change scale
     *
     * @param yLog if yLog is true scale is logarithmic, otherwise linear
     */
    public void yLog(boolean yLog) {
        this.yLog = yLog;
    }

    /**
     * Change dataset
     *
     * @param dataset for scatter graph
     */
    public void changeDataset(Graph dataset) {
        this.dataset = dataset;
    }

    /**
     * Change coloring
     *
     * @param colorColumnName for coloring graph. Usualy is penalty column name
     */
    public void changeColor(String colorColumnName) {
        this.colorColumnName = colorColumnName;
    }

    private void getScattersLayout() {
        LayoutWithAxis layout = new LayoutWithAxis();
        layout.setxAxisTitle(xName + " [" + GraphManager.myHashMap.get("units").get(GraphManager.myHashMap.get("normal_name").indexOf(xName)) + "]");
        layout.setyAxisTitle(yName + " [" + GraphManager.myHashMap.get("units").get(GraphManager.myHashMap.get("normal_name").indexOf(yName)) + "]");
        layout.setBackgroundColor(new Color(250, 250, 250));
        if (xLog)
            layout.getXaxis().setType("log");
        else
            layout.getXaxis().setType("linear");
        if (yLog)
            layout.getYaxis().setType("log");
        else
            layout.getYaxis().setType("linear");
        layout.disableLegend();
        this.layout = layout;

    }


    @SneakyThrows
    private void changeScatterDate() {
        int indexX = Graph.getIndexOfColumn(xName);
        int indexY = Graph.getIndexOfColumn(yName);
        ScatterData scatterData = new ScatterData();

        for (String[] item : dataset.getItems()) {
            Double x = Double.valueOf(item[indexX]);
            Double y = Double.valueOf(item[indexY]);
            scatterData.getData().addData(x, y);
        }
        scatterData.getMarker().setSize(2);

        List<String> currentColorColumnName = dataset.getColumn(colorColumnName);
        Stream<String> stream = currentColorColumnName.stream();
        List<Color> colorList = new ArrayList<>();
        long size = currentColorColumnName.size();
        List<Long> list = stream.map(s -> Double.valueOf(s).longValue()).sorted().collect(Collectors.toList());
        int indexOneThird = (int) size / 3;
        int indexTwoThird = 2 * (int) size / 3;
        long valueOneThird = list.get(indexOneThird);
        long valueTwoThird = list.get(indexTwoThird);

        for (String s : currentColorColumnName) {
            long l = Double.valueOf(s).longValue();
            if (l < valueOneThird) {
                colorList.add(Color.GREEN);
            } else if (l < valueTwoThird) {
                colorList.add(Color.YELLOW);
            } else {
                colorList.add(Color.RED);
            }
        }


        scatterData.setColors(colorList);
        this.scatterData = scatterData;
    }


}
