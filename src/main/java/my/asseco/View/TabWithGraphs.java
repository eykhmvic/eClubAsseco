package my.asseco.View;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import my.asseco.Model.Graph;
import my.asseco.Tools.GraphManager;

import javax.annotation.PostConstruct;

@UIScope
@SpringComponent
public class TabWithGraphs extends HorizontalLayout {

    private DatasetSelect datasetSelect;
    private NativeSelect<String> choosePenaltyParam;
    private ListSelect<String> xlist;
    private ListSelect<String> ylist;
    private ScatterGraph scatterGraph;
    private HistogramGraph righthistogramGraph;
    private HistogramGraph leftistogramGraph;
    private BoxPlotGraph boxPlotGraph;
    private HorizontalLayout firstRow;
    private HorizontalLayout secondRow;
    private CheckBox xlog = new CheckBox("X log");
    private CheckBox ylog = new CheckBox("Y log");

    @PostConstruct
    public void init() {
        setMargin(false);
        xlog.setValue(true);
        ylog.setValue(true);

        Button close = new Button("close");
        close.addClickListener(v -> {
            removeAllComponents();
            init();
        });

        if (GraphManager.emptyDatasets) {
            datasetSelect = new DatasetSelect(close);
            addComponent(datasetSelect);
            return;
        }
        choosePenaltyParam = new NativeSelect<>("Column for coloring");
        choosePenaltyParam.setItems(GraphManager.getMyHashMap().get("normal_name"));
        choosePenaltyParam.setEmptySelectionAllowed(false);
        choosePenaltyParam.setValue(GraphManager.getMyHashMap().get("normal_name").get(0));

        //x y list. columns for x or y axis
        xlist = new ListSelect<String>("X", GraphManager.getMyHashMap().get("normal_name"));
        ylist = new ListSelect<String>("Y", GraphManager.getMyHashMap().get("normal_name"));
        xlist.setWidth("90%");
        xlist.setRows(17);

        ylist.setWidth("90%");
        ylist.setRows(17);

        //vertical layout for x and y lists
        datasetSelect = new DatasetSelect(close);
        VerticalLayout xylist = new VerticalLayout(datasetSelect, choosePenaltyParam, new HorizontalLayout(xlist, xlog), new HorizontalLayout(ylist, ylog));
        xylist.setMargin(new MarginInfo(false, false));
        xylist.setSizeUndefined();

        //four graphs in a Graph tab
        scatterGraph = new ScatterGraph("amount clear", "command price", GraphManager.getFirstGraph());
        righthistogramGraph = new HistogramGraph("command price", GraphManager.getFirstGraph());
        leftistogramGraph = new HistogramGraph("amount clear", GraphManager.getFirstGraph());
        boxPlotGraph = new BoxPlotGraph("amount clear", "command price", GraphManager.getFirstGraph());


        // four graphs in two rows
        firstRow = new HorizontalLayout(scatterGraph.getGraph(), righthistogramGraph.getGraph());
        secondRow = new HorizontalLayout(leftistogramGraph.getGraph(), boxPlotGraph.getGraph());

        //two rows in one vertical layout
        VerticalLayout rows = new VerticalLayout(firstRow, secondRow);
        rows.setMargin(false);

        //resize components in this tab
        addComponents(xylist, rows);
        setExpandRatio(rows, 10);
        setExpandRatio(xylist, 1);
        addListener();

    }


    private void addListener() {
        xlist.addSelectionListener(event -> {
            if (event.getFirstSelectedItem().isPresent()) {
                String columnName = event.getFirstSelectedItem().get();
                firstRow.removeAllComponents();
                scatterGraph.changeX(columnName);
                firstRow.addComponents(scatterGraph.getGraph(), righthistogramGraph.getGraph());

                secondRow.removeAllComponents();
                leftistogramGraph.changeColumnName(columnName);
                boxPlotGraph.changeX(columnName);
                secondRow.addComponents(leftistogramGraph.getGraph(), boxPlotGraph.getGraph());
            }
        });
        xlog.addValueChangeListener(e -> {
            firstRow.removeAllComponents();
            scatterGraph.xLog(e.getValue());
            firstRow.addComponents(scatterGraph.getGraph(), righthistogramGraph.getGraph());

        });
        ylog.addValueChangeListener(e -> {
            firstRow.removeAllComponents();
            scatterGraph.yLog(e.getValue());
            firstRow.addComponents(scatterGraph.getGraph(), righthistogramGraph.getGraph());
        });
        ylist.addSelectionListener(event -> {
            if (event.getFirstSelectedItem().isPresent()) {
                String columnName = event.getFirstSelectedItem().get();
                firstRow.removeAllComponents();
                scatterGraph.changeY(event.getFirstSelectedItem().get());
                righthistogramGraph.changeColumnName(columnName);
                firstRow.addComponents(scatterGraph.getGraph(), righthistogramGraph.getGraph());

                secondRow.removeAllComponents();
                boxPlotGraph.changeY(columnName);
                secondRow.addComponents(leftistogramGraph.getGraph(), boxPlotGraph.getGraph());

            }
        });
        datasetSelect.addSelectionListener(singleSelectionEvent -> {
            if (singleSelectionEvent.getSelectedItem().isPresent()) {
                String selectedName = singleSelectionEvent.getSelectedItem().get();
                Graph newDataset = GraphManager.getGraph(selectedName);
                scatterGraph.changeDataset(newDataset);
                righthistogramGraph.setDataset(newDataset);
                leftistogramGraph.setDataset(newDataset);
                boxPlotGraph.setDataset(newDataset);

                firstRow.removeAllComponents();
                firstRow.addComponents(scatterGraph.getGraph(), righthistogramGraph.getGraph());
                secondRow.removeAllComponents();
                secondRow.addComponents(leftistogramGraph.getGraph(), boxPlotGraph.getGraph());
            }
        });
        choosePenaltyParam.addSelectionListener(singleSelectionEvent -> {
            scatterGraph.changeColor(singleSelectionEvent.getValue());
            firstRow.removeAllComponents();
            firstRow.addComponents(scatterGraph.getGraph(), righthistogramGraph.getGraph());

        });
    }
}
