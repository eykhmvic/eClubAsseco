package my.asseco.View;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import my.asseco.Tools.TimeSeriesManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;


@SpringView(name = "")
public class MainView extends VerticalLayout implements View {

    @Autowired
    MyNavigationBar myNavigationBar;

    @Autowired
    TabWithGraphs tabWithGraphs;

    @Autowired
    TabWithTimeSeries tabWithTimeSeries;

    @PostConstruct
    public void init() {
        setMargin(false);

        //main vertical layout
        VerticalLayout content = new VerticalLayout();
        addComponent(content);

        //add navigation bar
        content.addComponent(myNavigationBar);

        TabSheet tabSheetGraphTime = new TabSheet();
        tabSheetGraphTime.setHeight(100.0f, Unit.PERCENTAGE);
        tabSheetGraphTime.addStyleName(ValoTheme.TABSHEET_EQUAL_WIDTH_TABS);
        tabSheetGraphTime.setSizeFull();

        tabSheetGraphTime.addTab(tabWithGraphs, "Graph");
        tabSheetGraphTime.addTab(tabWithTimeSeries, "Time");

        content.addComponent(tabSheetGraphTime);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
