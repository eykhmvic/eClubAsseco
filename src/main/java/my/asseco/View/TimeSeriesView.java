package my.asseco.View;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.colorpicker.Color;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import my.asseco.Model.TimeSeries;
import org.kopi.plotly.PlotlyChart;
import org.kopi.plotly.PlotlyFactory;
import org.kopi.plotly.configuration.DataConfiguration;
import org.kopi.plotly.configuration.LayoutConfiguration;
import org.kopi.plotly.data.dataseries.CoupleOfData;
import org.kopi.plotly.data.features.modeData;
import org.kopi.plotly.data.types.LineData;
import org.kopi.plotly.layout.LayoutWithAxis;

import java.util.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class TimeSeriesView extends HorizontalLayout implements View {


    public HorizontalLayout getView() {
        removeAllComponents();
        ListSelect<String> ids = new ListSelect<String>("ID",
                dataset.getHashMap().keySet());
        ids.setRows(20);
        addComponent(ids);
        ids.setValue(Collections.singleton(dataset.getHashMap().keySet().iterator().next()));
        PlotlyChart chart = getGrarph(Collections.singleton(dataset.getHashMap().keySet().iterator().next()));
        addComponent(chart);

        ids.addSelectionListener(multiSelectionEvent ->
                {
                    if (multiSelectionEvent.getAllSelectedItems() != null) {
                        removeAllComponents();
                        addComponent(ids);
                        addComponent(getGrarph(multiSelectionEvent.getAllSelectedItems()));
                    }
                }
        );
        return this;
    }

    TimeSeries dataset;

    TimeSeriesView(TimeSeries dataset) {
        this.dataset = dataset;
    }

    @SneakyThrows
    private DataConfiguration getData(Set<String> ids) {
        DataConfiguration dataLine = new DataConfiguration();

        for (String s : ids) {
            LineData line = new LineData(s);

            line.setMode(modeData.LINES);
            CoupleOfData data1 = new CoupleOfData();
            List<String> list = dataset.getHashMap().get(s);
            for (int i = 0; i < list.size(); i++) {
                data1.addData(dataset.getDates().get(i), Double.valueOf(list.get(i)));
            }
            line.setDataSeries(data1);
            line.setColor(getRandomColor());
            dataLine.addDataConfiguration(line);

        }
        return dataLine;
    }

    private LayoutConfiguration getLayout() {
        LayoutWithAxis layoutwithAxis = new LayoutWithAxis();
        layoutwithAxis.setLayoutTitle("Time series Graph");
        layoutwithAxis.setyAxisTitle("amount in warehouse [count]");
        layoutwithAxis.disableXGrid();
        layoutwithAxis.setWidth(1200);
        layoutwithAxis.getXaxis().setType("date");
        return new LayoutConfiguration(layoutwithAxis);
    }

    private PlotlyChart getGrarph(Set<String> list) {
        return PlotlyFactory.renderChart(getData(list), getLayout());
    }

    private Color getRandomColor() {
        int R = (int) (Math.random() * 256);
        int G = (int) (Math.random() * 256);
        int B = (int) (Math.random() * 256);
        return new Color(R, G, B);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
