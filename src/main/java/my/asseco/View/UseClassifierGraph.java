package my.asseco.View;

import com.vaadin.data.Binder;
import com.vaadin.data.ValueProvider;
import com.vaadin.data.converter.StringToDoubleConverter;
import com.vaadin.event.FieldEvents;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Setter;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.SessionScope;


import java.util.HashMap;

import static my.asseco.Tools.Constants.COLUMNS_FOR_CLASSIFIER;
@Scope("prototype")
@SpringComponent
public class UseClassifierGraph extends VerticalLayout implements View {


    private HashMap<String, Double> nameValue = new HashMap<>();

    private Binder<HashMap<String, Double>> binder = new Binder<>();

    @lombok.Setter
    FieldEvents.FocusListener focusListener;


    public void init() {
        nameValue = new HashMap<>();
        for (String columnName : COLUMNS_FOR_CLASSIFIER) {
            nameValue.put(columnName, 0.0);
            TextField tf = new TextField(columnName);
            tf.addFocusListener(focusListener);
            binder.forField(tf)
                    .withConverter(new StringToDoubleConverter("Enter double")).bind(
                    (ValueProvider<HashMap<String, Double>, Double>) hashMap -> hashMap.get(columnName),
                    (Setter<HashMap<String, Double>, Double>) (hashMap, name) -> hashMap.put(columnName, name));
            addComponent(tf);
        }

        binder.setBean(nameValue);


    }

    public HashMap<String, Double> getHasMap() {
        return binder.getBean();
    }

    public void setHashMap(HashMap<String, Double> hashMap) {
        binder.setBean(hashMap);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
