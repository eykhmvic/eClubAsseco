package my.asseco;

import my.asseco.Tools.Constants;
import my.asseco.Tools.GraphManager;
import my.asseco.Tools.TimeSeriesManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Main.class)
@WebAppConfiguration
public class MainTests {

    @Test
    public void contextLoads() {
    }

    /**
     * Copy existing graph "hudba" as new garph. Add to DATASET_FOLDER and make sure, that new dataset exists.
     *
     * @throws IOException while reading files
     */
    @Test
    public void testAddClassifier() throws IOException {
        String testName = "test";
        String newFilePath = Constants.DATASET_FOLDER + testName + ".csv";
        Files.write(Paths.get(newFilePath), "something".getBytes());
        GraphManager.refreshDatasetNames();
        Assert.assertTrue(GraphManager.getDatasetNames().contains("test"));
        Files.delete(Paths.get(newFilePath));
    }

    /**
     * Copy existing graph "hudba" as new time series. Add to TIME_SERIES_FOLDER and make sure, that new time series exists.
     *
     * @throws IOException while reading files
     */
    @Test
    public void testAddTimeSeries() throws IOException {
        String testName = "test";
        String newFilePath = Constants.TIME_SERIES_FOLDER + testName + ".csv";
        Files.write(Paths.get(newFilePath),"something".getBytes());
        Assert.assertTrue(TimeSeriesManager.getTimeSeriesNames().contains("test"));
        Files.delete(Paths.get(newFilePath));
    }


    /**
     * Test serialize and deserialize statistical graphs
     */
    @Test
    public void testSerializeGraphs() {
        GraphManager graphManager = new GraphManager();
        Assert.assertTrue(graphManager.test());
    }

    /**
     * Test serialize and deserialize TimeSeries graphs
     */
    @Test
    public void testSerializeTimeSeries() {
        TimeSeriesManager timeSeriesManager = new TimeSeriesManager();
        Assert.assertTrue(timeSeriesManager.test());
    }



}
