import pickle
import random

from experiments.sacre_experiment import ex
from utils import ExperimentInfo, join_lt_dict
import pandas as pd

from my_tree_export import export_graphviz
import graphviz


def test_classifier(iterator, experiment_info: ExperimentInfo, construct_function, export_function=None, name: str = "",
                    scaler=None):
    lt_dic_arr = dict()
    x_arr = []
    export = []
    mse_arr = []
    print(experiment_info)

    for it in iterator:
        d = experiment_info._asdict().copy()
        d["classifier_name"] = construct_function.__name__
        d["construct_function"] = construct_function
        d["it"] = it
        d["manufacture_command_semi"] = experiment_info.company + "/" + "manufacture_command_semi.csv"
        # print(columns)
        d["name"] = name
        d["random"] = random.random() * 10
        d["scaler"] = scaler
        # print(d)
        run = ex.run(config_updates=d)
        run.add_resource(d["manufacture_command_semi"])
        lt_dic, msev, x, cls, test_data = run.result
        test_data = pd.read_msgpack(test_data)
        cls = pickle.loads(cls)
        x_arr.append(x)
        mse_arr.append(msev)

        lt_dic_arr = join_lt_dict(lt_dic, lt_dic_arr)

        if export_function:
            export.append(export_function(cls, name + str(it), experiment_info, test_data, scaler))

    return lt_dic_arr, mse_arr, x_arr, export


def export_tree(classifier, name, experiment_info, test_data, scaler):
    df_copy = pd.DataFrame(index=[1], columns=test_data.columns)
    columns = experiment_info["columns"]
    tree_data = export_graphviz(classifier, out_file=None, feature_names=columns, filled=True,
                                test_data=test_data, scaler=scaler, columns=columns,
                                experiment_info=experiment_info)
    path = str(name)

    # print(tree_data)
    graph = graphviz.Source(tree_data, format="png")
    graph.render(path)
    return graph.source
