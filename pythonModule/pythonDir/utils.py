import ast
import datetime
import locale
import math
import re
import textwrap
import time
from typing import List, Callable, Dict

import matplotlib.pyplot as plt
import pandas as pd

from error_functions import *
from experiment_info import ExperimentInfo

millnames = ['', 'k', 'M', 'G', 'T']

DOT_REPLACE = "{_}"

TRANS_PEN = "trans_cost_penalty"


def lt(a, b):
    "Same as a < b."
    return a < b


def le(a, b):
    "Same as a <= b."
    return a <= b


def eq(a, b):
    "Same as a == b."
    return a == b


def ne(a, b):
    "Same as a != b."
    return a != b


def ge(a, b):
    "Same as a >= b."
    return a >= b


def gt(a, b):
    "Same as a > b."
    return a > b


def millify(n, decimal=2) -> str:
    """
    Format number with using k, M, G, T prefix
    :param n: number
    :param decimal: number of digit after dot
    :return: formated number
    """
    n = float(n)
    millidx = max(0, min(len(millnames) - 1,
                         int(math.floor(0 if n == 0 else math.log10(abs(n)) / 3))))

    return ('{:.' + str(decimal) + 'f}{}').format(n / 10 ** (3 * millidx), millnames[millidx])


def millify_string(n, decimal: int = 0, split="([\W+,_])"):
    """
    Format all decimal number in string by millify.
    :param n: string containing number
    :param decimal: number of digit after dot
    :param split: see here https://docs.python.org/3/library/re.html#re.split 
    :return: 
    """
    arr = re.split(split, n)
    return "".join([millify(int(it), decimal) if it.isdecimal() else it for it in arr])


def atoi(text):
    return int(text) if text.isdigit() else text


def natural_keys(text):
    """
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    """
    return [atoi(c) for c in re.split('(\d+)', text)]


def split_dataset(df: pd.DataFrame, train_years: List[int]) -> (pd.DataFrame, pd.DataFrame, pd.DataFrame):
    """
    Split dataset into train, test and validation part.
    :param df: dataset to split
    :param train_years: 
    :return: 
    """
    np.random.seed(7)
    train_data, not_train_data = get_years(df, train_years)
    msk = np.random.rand(len(not_train_data)) < 1.1
    test_data = not_train_data[msk]
    validation = not_train_data[~msk]
    return train_data, test_data, validation


# vybrání dvou let jako datasetu (první trénovací, druhý testovací)
def year_start(year: int):
    """
    Return start of year in timestamp.
    """
    return int(datetime.datetime(year=year, month=1, day=1).timestamp())


def get_datasubset(manufacture_command_semi_name, train_years, test_years, original_penalty, transformation, filters,
                   remove_na=True, **args):
    """
    Return subset of input file filtered by year and filter, with calculated transformed penalty.
    :param manufacture_command_semi_name: 
    :param train_years: 
    :param test_years: 
    :param original_penalty: 
    :param transformation: 
    :param filters: 
    :param remove_na: Remove NA value from returned dataframe
    :param args: 
    :return: 
    """
    data_subset = pd.read_csv(manufacture_command_semi_name)

    t_t_years = train_years.copy()
    t_t_years.extend(test_years)

    data_subset, _ = get_years(data_subset, t_t_years)

    data_subset["_penalty"] = data_subset[original_penalty]
    data_subset[TRANS_PEN] = get_transformed_column(data_subset[original_penalty], transformation)

    if remove_na:
        data_subset = data_subset.dropna()

    out = filter_dataframe(data_subset, filters)
    return out


def get_years(df: pd.DataFrame, years: List[int], feature: str = "planed_end"):
    """
    Return row of dataframe with timestamp at feature in years.
    :param df: 
    :param years: array of year 
    :param feature: 
    :return: 
    """
    filters = np.zeros((df.shape[0]), dtype=bool)

    for year in years:
        sy = year_start(year)
        ey = year_start(year + 1)
        filters = filters | (df[feature] > sy) & (df[feature] < ey)

    return df[filters], df[~filters]


def join_lt_dict(lt_dic: Dict, lt_dic_arr: Dict):
    """
    Append values from lt_dic to corresponding array from lt_dict_arr.
    :param lt_dic_arr: 
    :param lt_dic: 
    :return: 
    """
    for key, value in lt_dic.items():
        if key not in lt_dic_arr:
            lt_dic_arr[key] = []
        if isinstance(value, collections.Iterable):
            lt_dic_arr[key].extend(value)
        else:
            lt_dic_arr[key].append(value)
    return lt_dic_arr


def display_classifier_graph(x_arr: list, mse_arr: list, lt_dic: dict, title: str, xlabel: str,
                             experiment_info: ExperimentInfo):
    """
    Display graph of classification error
    :param x_arr: array to x axis 
    :param mse_arr: array with mse
    :param lt_dic: dictionary with y values arrays
    :param title: name of classifier
    :param xlabel: xlabel of graph
    :param experiment_info: 
    :return: 
    """
    # plt.figure()

    fig, ax = plt.subplots(figsize=(10, 6))
    for lim, value in sorted(lt_dic.items(), key=lambda x: natural_keys(x[0])):
        marker = '-X'
        if "train" in lim:
            marker = "-P"
        plt.plot(x_arr, value, marker, label="less_than_" + millify_string(lim))

    plt.gca().set_position((.1, .25, .8, .6))

    plt.xlabel(xlabel)
    plt.ylabel('accuracy')
    plt.title(title)
    plt.figtext(0.0, 0.0, generate_experiment_info(experiment_info))

    ax2 = ax.twinx()
    ax2.set_position((.1, .25, .8, .6))
    ax2.plot(x_arr, mse_arr, "-ok", label="mse")
    ax2.set_ylabel('mse', color='k')
    ax2.tick_params('y', colors='k')

    ax.legend(bbox_to_anchor=(1.1, 1), loc=2, shadow=True)

    plt.show()


def generate_experiment_info(experiment_info):
    """
    Experiment description are constructed in this method.
    :param experiment_info: 
    :return: 
    """
    locale.setlocale(locale.LC_ALL, '')
    if "filters" not in experiment_info:
        experiment_info["filters"] = ""
    out = """train data: {}
test data: {}
penalty: {}
transformation: {}
filters: {}
columns: {}
generation date: {}
                """.format(experiment_info["train_years"], experiment_info["test_years"], experiment_info["original_penalty"],
                           experiment_info["transformation"], filter_to_str(experiment_info["filters"], True),
                           "\n".join(textwrap.wrap(", ".join(experiment_info["columns"]), 120)),
                           time.strftime("%c")
                           )
    return out


# Vykreslí scatter plot pro zadané data a příznaky. Grafy obarví podle zadaného příznaku. Na diagonále vykreslí histogramy.

eps = 0.000001


def get_scaled_column(col, log=False, max_val=False):
    col = col.copy()
    if max_val:
        # print("replace values, count:", col[col > max_val].shape[0], "maximum:", col.max(), "replaced by", max_val)
        col[col > max_val] = max_val
    col = col - col.min()
    if log:
        col = np.log(col + 1)
    col = col - col.min()
    return col / col.max()


def get_transformed_column(col, transformation):
    """
    Return transformed column. On column was applicated transformation from @transformation in order. 
    col: pandas column
    transformation: array of transformations tuples (transformation, parameters)
    """
    col = col.copy()
    for tr, par in transformation:
        tr = tr.lower()
        if tr == "to_categorical":
            par = sorted(par)
            cc = np.zeros(col.shape, dtype=int)
            for lim in par:
                cc[col > lim] = cc[col > lim] + 1
            col = cc

        if tr == "to_categorical_percentage":
            par = sorted(par)
            cc = np.zeros(col.shape, dtype=int)
            for lim in par:
                lim = np.nanpercentile(col, lim)
                cc[col > lim] = cc[col > lim] + 1
            col = cc

        if tr == "max":
            # print("replace values, count:", col[col > par].shape[0], "maximum:", col.max(), "replaced by", par)
            col[col > par] = par
        if tr == "min_to_zero":
            col = col - col.min()
        if tr == "add_eps":
            col = col + par
        if tr == "min_to_one":
            col = col - col.min() + 1
        if tr == "log":
            col = np.log(col) / np.log(par)
        if tr == "scale_to_one":
            if not par:
                par = col.max()
            col = col / par
    return col


def get_standart_transformation(log: float = False, max_val: float = False, color_max=None):
    """Return predefined transformation, this transformation should be configured by max value and log value (neither, either or both).
    :param log: 
    :param max_val: 
    :param color_max:
    :return: 
    """
    arr = []
    if max_val:
        arr.append(("max", max_val))
    if log:
        # arr.append(("min_to_one", None))
        arr.append(("add_eps", 1))
        arr.append(("log", log))
    # arr.append(("min_to_zero", None))
    arr.append(("scale_to_one", color_max))
    # print("color transformation: ", arr)
    return arr


def get_color_for_column(col, transformation):
    """
    Function return color for column, on column is used transformation. Output is compatible with matplotlib.
    col: pandas column
    transformation: transformation of column to function get_transformed_column, you can get take from get_standart_transformation
    """
    colormap = plt.cm.gist_ncar
    col = get_transformed_column(col, transformation)
    # col = get_scaled_column(col, log, max_val)
    return colormap(0.4 + col * 0.35)


def get_color_for_column_alpha(col, alpha_col):
    """deprecated"""
    colormap = plt.cm.gist_ncar
    colors = colormap(0.4 + get_scaled_column(col) * 0.35)
    colors[:, 3] = get_scaled_column(alpha_col)
    return colors


def multi_scatter(data, cols, color_col, log_color=True):
    """

        log_color: color column logaritmized before use
    """
    sub = len(cols)
    fig = plt.figure(figsize=(8 * sub, 8 * sub))

    ii = 1
    for row in cols:
        for col in cols:
            if row == col:
                fig.add_subplot(sub, sub, ii)
                plt.hist(data[row].dropna(), bins=100)
                plt.title("hist " + row)
                #                 plt.xlabel(row)
                #                  plt.ylabel(col)
                pass
            else:
                fig.add_subplot(sub, sub, ii)
                m = np.min(data[color_col]) - 1
                # plt.scatter(data[row], data[col], c=get_color_for_column(np.log(data[color_col] - m)))
                # if log_color:
                #    col_col = np.log(data[color_col] - m)
                # else:
                #    col_col = data[color_col]

                trans = get_standart_transformation(log=False, max_val=False)
                col_col = get_color_for_column(data[color_col], trans)
                plt.scatter(data[row], data[col], c=col_col)
                # plt.scatter(data_cont_en[row], data_cont_en[col], 'r.')
                plt.title(row + " " + col)
                plt.xlabel(row)
                plt.ylabel(col)
            ii = ii + 1

    plt.show()


def multi_scatter_alpha(data, cols, color_col, alpha_col, log_color=True):
    """
        deprecated
        log_color: color column logaritmized before use
    """
    sub = len(cols)
    fig = plt.figure(figsize=(8 * sub, 8 * sub))

    ii = 1
    for row in cols:
        for col in cols:
            if row == col:
                fig.add_subplot(sub, sub, ii)
                plt.hist(data[row].dropna(), bins=100)
                plt.title("hist " + row)
                #                 plt.xlabel(row)
                #                  plt.ylabel(col)
                pass
            else:
                fig.add_subplot(sub, sub, ii)
                m = np.min(data[color_col]) - 1
                # plt.scatter(data[row], data[col], c=get_color_for_column(np.log(data[color_col] - m)))
                if log_color:
                    col_col = np.log(data[color_col] - m)
                else:
                    col_col = data[color_col]
                plt.scatter(data[row], data[col], c=get_color_for_column_alpha(col_col, alpha_col))
                # plt.scatter(data_cont_en[row], data_cont_en[col], 'r.')
                plt.title(row + " " + col)
                plt.xlabel(row)
                plt.ylabel(col)
            ii = ii + 1

    plt.show()


def day_sinus_converter(inp, offset, period):
    mm = max(inp)
    offset = -offset * mm
    period = period * mm * 2
    return map(lambda x: math.sin(math.radians(
        (period / 2 if x + offset > period / 2 else 0 if x + offset < 0 else x + offset) * 90 / (period / 2))), inp)


def calculate_penalty(data, row=None, weight=None, func=None):
    if weight:
        data = [x * y for x, y in zip(data, weight)]
    if func:
        data = map(func, zip(data, infinite_iterator(row)))
    return sum(data)


def gen_sin_weight(days, offset, period):
    return day_sinus_converter(range(days), offset, period)


def gen_seven_zero_weight(days):
    return [0] * 7 + [1] * (days - 7)


def gen_one_weight(days):
    return [1] * (days)


def gen_seven_decimated_weight(days):
    return [0.1] * 7 + [1] * (days - 7)


def gen_classic_pen(it):
    return 1 if it[0] > 20000 else 0


def gen_day_pen(it):
    sp = it[0]  # stock price
    r = it[1]  # row
    return max(0, (sp - r.actual_cost) / r.command_price)


def task_end_in_time(df, day, offset):
    end_date = datetime.datetime.fromtimestamp(day) + datetime.timedelta(days=offset)
    d = int(end_date.timestamp())
    df2 = df[(df.ukonceni_X < d)]
    return df2.kusy_ciste.count(), df2.kusy_ciste.sum()


def smallest(column: np.ndarray, k: int) -> np.ndarray:
    """
    Return k smallest items in column.
    :param column: feature
    :param k: count of marked items
    :return: one on position of smallest items, zero otherwise
    """
    ind = np.argpartition(column, k)[:k]
    out = np.zeros(column.shape)
    out[ind] = 1
    return out


def highest(column: np.ndarray, k: int) -> np.ndarray:
    """
    Return k highest items in column.
    :param column: feature
    :param k: count of marked items
    :return: one on position of highest items, zero otherwise
    """
    ind = np.argpartition(column, -k)[-k:]
    out = np.zeros(column.shape)
    out[ind] = 1
    return out


def filter_dataframe(df: pd.DataFrame, filters: List[List[Tuple[str, Callable, float]]]) \
        -> (pd.DataFrame, pd.DataFrame, np.ndarray):
    """
    Filter dataframe with formula in DNF
    :param df: dataframe to filter
    :param filters: formula in DNF 
    :return: 
    """
    out = df
    primary = np.zeros(out.shape[0], dtype=np.bool)
    for ff in filters:
        it = np.ones(out.shape[0])
        for indicator, op, value in ff:
            opp = op(out[indicator], value)
            # print(opp)
            it = np.logical_and(it, opp)
        primary = np.logical_or(primary, it)
    return out[primary], out[np.logical_not(primary)], primary


def filter_to_str(filters: List[List[Tuple[str, Callable, float]]], hr=False) -> str:
    func = str
    up = ","
    down = ","
    if hr:
        func = millify
        up = "|"
        down = "&"
    return "[" + up.join(
        ["[" + down.join(["({},{},{})".format(ind, op.__name__, func(val)) for ind, op, val in ff]) + "]"
         for ff in filters]) + "]"


def plot_color_pallet():
    X = [i for i in range(0, 24)]
    Y = [i for i in range(0, 24)]
    labels = range(1, len(X) + 1)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    for x, y, lab in zip(X, Y, labels):
        ax.scatter(x, y, label=lab)

    colormap = plt.cm.gist_ncar  # nipy_spectral, Set1,Paired
    colorst = [colormap(i) for i in np.linspace(0.4, 0.75, len(ax.collections))]
    for t, j1 in enumerate(ax.collections):
        j1.set_color(colorst[t])

    ax.legend(fontsize='small')
    plt.show()

    # get_scaled_column(manufacture_command_semi_full.penalty).describe()


def to_value(l):
    return ast.literal_eval(l.lower().strip().title())


def get_mongo_key(it):
    return str(it).replace(".", "{#}")


def infinite_iterator(it):
    while True:
        yield it
