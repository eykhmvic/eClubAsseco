import csv
import datetime

import pyodbc

server = ""
database = ""
username = ""

db = "hudba"
skup_zbo = ""
price_filter = ""

if db == "pribory":
    server = 'eclub-sql.ciirc.cvut.cz'
    database = 'Helios001'
    username = 'eclub'
    skup_zbo = "(SkupZbo >= 200 AND SkupZbo < 300)"

if db == "lepidlo":
    server = 'lion2\sql2014'
    database = 'MJA_CVUT4'
    username = 'michal'

if db == "letadla":
    server = 'JELINKOVAPE-NB2\\SQL2016'
    database = 'HeliosCvicna1'
    username = 'Michal'
    skup_zbo = "(" + " or ".join("SkupZbo=" + str(it) for it in [210, 211, 230, 250, 270, 271, 272, 291]) + ")"
    price_filter = "where ((TabZKalkulace.zmenaOd=289))"

if db == "hudba":
    server = 'JELINKOVAPE-NB2\\SQL2016'
    database = 'HeliosCvicna2'
    username = 'Michal'
    skup_zbo = "(" + " or ".join("SkupZbo=" + str(it) for it in ["001", "003", "004"]) + ")"
    price_filter = "where ((TabZKalkulace.zmenaOd=11))"

password = 'Helios100953'
# password = 'Di1YoajZuUL29KO8CzVQ'
driver = '{ODBC Driver 13 for SQL Server}'
cnxn = pyodbc.connect(
    'DRIVER=' + driver + ';PORT=1433;SERVER=' + server + ';PORT=1443;DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
cursor = cnxn.cursor()

# vyrobni_slozitost = "SELECT nazev1, nazev, TAC, RegCis as id  FROM TabPostup " + \
#                     "LEFT OUTER JOIN TabKmenZbozi VPostupKmenZbozi ON TabPostup.dilec=VPostupKmenZbozi.ID"


vyrobni_slozitost = """SELECT
VPostupKmenZbozi.Nazev1 AS nazev1,
tabPostup.nazev,
tabPostup.tac_N AS TAC,
VPostupKmenZbozi.CisloZbozi AS id
FROM TabPostup
  LEFT OUTER JOIN TabCzmeny VPostupCZmenyOd ON TabPostup.ZmenaOd=VPostupCZmenyOd.ID
  LEFT OUTER JOIN TabCzmeny VPostupCZmenyDo ON TabPostup.ZmenaDo=VPostupCZmenyDo.ID
  LEFT OUTER JOIN TabKmenZbozi VPostupKmenZbozi ON TabPostup.dilec=VPostupKmenZbozi.ID
  LEFT OUTER JOIN TabCPraco VPostupCPraco ON TabPostup.pracoviste=VPostupCPraco.ID
WHERE
(((TabPostup.IDZakazModif IS NOT NULL OR  VPostupCZmenyOd.platnostTPV=1 AND VPostupCZmenyOd.datum<=GETDATE() AND  (VPostupCZmenyDo.platnostTPV=0 OR TabPostup.ZmenaDo IS NULL OR (VPostupCZmenyDo.platnostTPV=1 AND VPostupCZmenyDo.datum>GETDATE())) )))
"""

stock_price = """
SELECT
  VZKalkulaceKmenZbozi.CisloZbozi AS id,
  TabZKalkulace.Celkem AS manufacture_cost
FROM TabZKalkulace
  LEFT OUTER JOIN TabKmenZbozi VZKalkulaceKmenZbozi ON TabZKalkulace.Dilec=VZKalkulaceKmenZbozi.ID
{}
""".format(price_filter)

stock_load = """
              SELECT
              KMEN.CisloZbozi                                    AS id,
              KMEN.Nazev1                                    AS product_name,
              KMEN.SkupZbo                                   AS store_group,
              POHYBY.druhPohybuZbo                           AS load_unload,
              POHYBY.mnozstvi                                AS amount,
              POHYBY.mnozstviDruhove                  AS sign_amount,
              POHYBY.skutecneDatReal_X                AS date,
              POHYBY.skutecneDatReal_Y                AS year,
              POHYBY.skutecneDatReal_M                AS month,
              POHYBY.skutecneDatReal_D                AS day,
              POHYBY.CisloZakazky AS order_id
              
              FROM TabStavSkladu SKLAD JOIN TabKmenZbozi KMEN ON SKLAD.IDkmenZbozi=KMEN.ID
              INNER JOIN TabPohybyZbozi AS POHYBY ON POHYBY.IDZboSklad = SKLAD.ID
              WHERE POHYBY.DruhPohybuZbo >= 0 AND POHYBY.DruhPohybuZbo <= 4 AND KMEN.Dilec = 1                   
                             AND DruhPohybuZbo >= 0 AND DruhPohybuZbo <= 4 
"""

manufacture_command = """
SELECT
TabPrikaz.ID AS idd,
TabPrikaz.kusy_ciste AS amount_clear,
TabPrikaz.kusy_zad AS amount,
VPrikazKmenZbozi.SkupZbo AS store_group,
VPrikazKmenZbozi.CisloZbozi AS product_id,
VPrikazKmenZbozi.Nazev1 AS product_name,
VPrikazZakazka.ID as order_id,
tabPrikaz.plan_zadani AS planed_start,
tabPrikaz.plan_ukonceni AS planed_end,
tabPrikaz.zadani_X AS start_date,
tabPrikaz.ukonceni_X AS end_date,
tabPrikaz.kusy_odved AS amount_produced,
tabPrikaz.kusy_zmet AS amount_rejected,
(SELECT P.RadaPrikaz FROM TabPrikaz P WHERE P.ID=TabPrikaz.IDPrikazVyssi) as superior_command
FROM TabPrikaz
LEFT OUTER JOIN TabKmenZbozi VPrikazKmenZbozi ON VPrikazKmenZbozi.ID=TabPrikaz.IDTabKmen
LEFT OUTER JOIN TabZakazModifDilce VPrikazZakazModifDilce ON VPrikazZakazModifDilce.IDZakazModif=TabPrikaz.IDZakazModif AND VPrikazZakazModifDilce.IDKmenZbozi=TabPrikaz.IDTabKmen
LEFT OUTER JOIN TabZakazka VPrikazZakazka ON VPrikazZakazka.ID=TabPrikaz.IDZakazka
where {}
""".format(skup_zbo)

material = """SELECT
TabKvazby.ID,
/*#TabKvazby.vyssi,
TabKvazby.nizsi,*/
VKVazbyVyssiKmenZbozi.CisloZbozi AS id,
VKVazbyVyssiKmenZbozi.nazev1 AS name,
VKVazbyNizsiKmenZbozi.material,
VKVazbyNizsiKmenZbozi.CisloZbozi AS id_down,
VKVazbyNizsiKmenZbozi.nazev1 AS name_down,
TabKvazby.mnozstvi AS amount
FROM TabKvazby
  LEFT OUTER JOIN TabCzmeny VKVazbyZmenaOdCZmeny ON TabKvazby.ZmenaOd=VKVazbyZmenaOdCZmeny.ID
  LEFT OUTER JOIN TabCzmeny VKVazbyZmenaDoCZmeny ON TabKvazby.ZmenaDo=VKVazbyZmenaDoCZmeny.ID
  LEFT OUTER JOIN TabKmenZbozi VKVazbyVyssiKmenZbozi ON VKVazbyVyssiKmenZbozi.ID=TabKvazby.vyssi
  LEFT OUTER JOIN TabZakazModif VKVazbyZakazModif ON VKVazbyZakazModif.ID=TabKvazby.IDZakazModif
  LEFT OUTER JOIN TabCVariant VKVazbyVyssiCVariant ON VKVazbyVyssiCVariant.ID=TabKvazby.IDVarianta
  LEFT OUTER JOIN TabKmenZbozi VKVazbyNizsiKmenZbozi ON TabKvazby.nizsi=VKVazbyNizsiKmenZbozi.ID
WHERE
(((TabKvazby.IDZakazModif IS NOT NULL OR VKVazbyZmenaOdCZmeny.platnostTPV=1 AND VKVazbyZmenaOdCZmeny.datum<=GETDATE() AND (VKVazbyZmenaDoCZmeny.platnostTPV=0 OR TabKvazby.ZmenaDo IS NULL OR (VKVazbyZmenaDoCZmeny.platnostTPV=1 AND VKVazbyZmenaDoCZmeny.datum>GETDATE())) )))
"""


def get_query(query, name='zakazky.csv', to_timestamp=True):
    # query = query + " order by skutecneDatReal_X"
    print(query)
    cursor.execute(query)

    with open(name, 'w', newline='', encoding='utf-8') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',', quotechar='\"', quoting=csv.QUOTE_NONNUMERIC)

        row = cursor.fetchone()

        csv_writer.writerow([x[0] for x in row.cursor_description])
        refactor_row(row, to_timestamp)
        csv_writer.writerow(row)

        i = 0
        for row in cursor.fetchall():
            # if any(map(lambda x: "\"" in str(x), row)):
            # print("ERROR, row contains \"", row)
            if refactor_row(row, to_timestamp):
                csv_writer.writerow(row)
            i += 1


def refactor_row(row, to_timestamp):
    if to_timestamp:
        for j, it in enumerate(row):
            if isinstance(it, datetime.date):
                if row[j].year > 2038:
                    return False
                row[j] = row[j].timestamp()

    return True


print(stock_load)

db_name = db + "/"

get_query(vyrobni_slozitost, db_name + "slozitost.csv")
get_query(stock_price, db_name + "stock_price.csv")
get_query(stock_load, db_name + "stock_load.csv")
get_query(manufacture_command, db_name + "manufacture_command.csv")
get_query(material, db_name + "material.csv")
# get_query(stock_load + " and RegCis='6815200210'", "stock_load_9608022710.csv")
# get_query(stock_load + " and RegCis='3326295100'", "stock_load_3326295100.csv")
# get_query(stock_load + " and RegCis='2623761000'", "stock_load_2623761000.csv")
# get_query(stock_load + " and RegCis='0663266035'", "stock_load_0663266035.csv")
# get_query(stock_load + " and RegCis='0000373116'", "stock_load_0000373116_n.csv")
# get_query(stock_load + " and KMEN.RegCis='3619066001' ", "stock_load_3619066001_nn.csv")
# get_query(stock_load + " and nazev1='TORX-Schraube mit Senkkopf M 4x12-1.4301'", "stock_load_0000373116_name.csv")
# row with " in name
# get_query(stock_load + " and RegCis='3322921000'", "stock_load_one.csv")
