from utils import gen_sin_weight, gen_classic_pen, gen_seven_zero_weight, gen_seven_decimated_weight, gen_one_weight, \
    gen_day_pen, lt_percentage_err, lt_log_err, lt_err

GENERAL_FOLDER_FOR_COMPANIES = "manufacture_command_ex"

COMPANY_NAME = "pribory"
# COMPANY_NAME = "letadla"
# COMPANY_NAME = "hudba"

# calculate_penalty

# from prepare_data import calculate_manufacture_command_item

LENGTH_ARR = [60, 100]
_WEIGHT_FUNCTION = {"cost_sin_1_7": (lambda days: gen_sin_weight(days, 0.1, 0.7), None),
                    "cost_sin_2_6": (lambda days: gen_sin_weight(days, 0.2, 0.6), None),
                    "cost_penalty": (gen_one_weight, None),
                    "penalty": (gen_one_weight, gen_classic_pen),
                    "cost_penalty_seven_zero": (gen_seven_zero_weight, None),
                    "cost_penalty_seven_decimated": (gen_seven_decimated_weight, None),
                    "cost_sin_days_2_6": (lambda days: gen_sin_weight(days, 0.2, 0.6), gen_day_pen),
                    "penalty_days": (gen_one_weight, gen_day_pen),
                    }

NAME_MAP = {it + "_60": it for it in _WEIGHT_FUNCTION.keys()}

WEIGHT_DICT = {(str(n) + "_" + str(d)): (None if f[0] is None else list(f[0](d)), f[1]) for n, f in
               _WEIGHT_FUNCTION.items() for d in LENGTH_ARR}

CLS_CONF = dict()
conf_ = dict()
conf_["category_limit"] = [(lt_percentage_err, (0.1, 50000)), (lt_percentage_err, (0.2, 50000)), (lt_err, 100000),
                           (lt_err, 250000), (lt_err, 1000000), (lt_log_err, (1, 10))]
