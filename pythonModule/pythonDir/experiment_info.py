import collections

ExperimentInfo = collections.namedtuple("ExperimentInfo",
                "train_years test_years original_penalty penalty transformation category_limit company filters columns")
