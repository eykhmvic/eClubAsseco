"""
File implementing read parameter from input and run experiment.
"""

VERSION = 10

import logging
import pickle
import sys

import jsonpickle as json
import pandas as pd
from sacred import Experiment
from sacred.observers import MongoObserver
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import DecisionTreeRegressor
from sklearn.base import clone

from config import COMPANY_NAME
from config import GENERAL_FOLDER_FOR_COMPANIES
from test_cls import export_tree
from utils import get_datasubset, lt_err, lt_log_err, lt_percentage_err, get_mongo_key, exact_acc, split_dataset, \
    TRANS_PEN, DOT_REPLACE, mse_err, filter_dataframe, filter_to_str
from utils import le, gt

ex = Experiment("asseco penalty estimation", interactive=True)

ex.observers.append(MongoObserver.create(
    url='mongodb://asseco_sacred:Mai0eiYeiZ@biodata.felk.cvut.cz:27001',
    db_name='asseco_sacred', connectTimeoutMS=None, serverSelectionTimeoutMS=180000, connect=False))

ll = logging.getLogger("asd")
ll.setLevel(logging.WARNING)

ex.logger = ll


def construct_tree(it):
    return DecisionTreeRegressor(max_depth=it), it


def construct_random_forest(it):
    return RandomForestRegressor(n_estimators=it[0]), it


classifiers = [
    KNeighborsClassifier(1),
    KNeighborsClassifier(2),
    KNeighborsClassifier(3),
    KNeighborsClassifier(4),
    KNeighborsClassifier(5),
    KNeighborsClassifier(6),
    KNeighborsClassifier(7),
    SVC(kernel="linear", C=0.025),
    SVC(gamma=2, C=1),
    DecisionTreeClassifier(max_depth=5),
    DecisionTreeClassifier(max_depth=6),
    DecisionTreeClassifier(max_depth=7),
    DecisionTreeClassifier(max_depth=8),
    RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
    RandomForestClassifier(max_depth=5, n_estimators=15, max_features=1),
    RandomForestClassifier(max_depth=5, n_estimators=20, max_features=1),
    RandomForestClassifier(max_depth=8, n_estimators=10, max_features=1),
    RandomForestClassifier(max_depth=8, n_estimators=15, max_features=1),
    RandomForestClassifier(max_depth=8, n_estimators=20, max_features=1),
    MLPClassifier(alpha=1),
    AdaBoostClassifier(),
    GaussianNB(),
    QuadraticDiscriminantAnalysis()]


# GaussianProcessClassifier(1.0 * RBF(1.0)),


def standard_classifiers(it):
    return clone(classifiers[it]), it


@ex.config
def config():
    category_limit = None
    company = None
    train_years = None
    test_years = None
    original_penalty = None
    penalty = None
    transformation = []
    filters = None
    construct_function = None
    classifier_name = None
    cls_dict = None
    category_limit = None
    columns = None
    name = None
    manufacture_command_semi = None
    random = None
    scaler = None
    err_fea = None
    name = None
    relevant = None
    cls_split_str = None
    pen_delimiter = None
    experiment_info = None
    company = None
    text_info = """connect"""
    version = VERSION
    exp_name = None
    exp_pbs_id = None


def export_classifier(fin_class, exp_name, exp_pbs_id, columns, scaler, experiment_info, data_subset):
    to_save = dict()
    to_save["classifier"] = fin_class
    to_save["columns"] = columns
    to_save["scaler"] = scaler

    fn = exp_name + ".cls" + exp_pbs_id
    file = open(fn, "wb")
    pickle.dump(to_save, file)

    for i, part in enumerate(fin_class):
        experiment_export_tree(data_subset, experiment_info, fn, i, part, scaler)


def experiment_export_tree(data_subset, experiment_info, fn, i, part, scaler):
    fil, cls = part
    if isinstance(cls, DecisionTreeClassifier):
        experiment_info_loc = experiment_info.copy()
        experiment_info["filters"] = fil
        export_tree(cls, fn + "tree" + str(i), experiment_info_loc, data_subset, scaler)


@ex.main
def run(cls_dict: dict, category_limit, original_penalty, columns, manufacture_command_semi, train_years,
        test_years, transformation, scaler, err_fea, pen_delimiter, exp_name, exp_pbs_id, experiment_info):
    """
    Take ona dividing of state space and classifiers (describe in cls_dict) and evaluate.
    :param cls_dict:
    :param category_limit:
    :param original_penalty:
    :param columns:
    :param manufacture_command_semi:
    :param train_years:
    :param test_years:
    :param transformation:
    :param scaler:
    :param err_fea:
    :param pen_delimiter:
    :return:
    """
    print([it[0] for it in cls_dict])
    subset_using = pd.Series()
    results = dict()
    # results_examples = dict()

    data_subset, _, subset_filter = get_datasubset(manufacture_command_semi, train_years, test_years, original_penalty,
                                                   transformation, [[]])

    print("pen >1M", (data_subset["_penalty"] > 1000000).sum(), len(data_subset))

    scaler.fit(data_subset[columns])

    features = []

    fin_class = []

    for filters, classifiers in cls_dict:
        print(filters)
        features, min_cls, min_res, subset_filter = find_optimal_classifier_for_filter(category_limit, classifiers,
                                                                                       columns, err_fea, features,
                                                                                       filters,
                                                                                       manufacture_command_semi,
                                                                                       original_penalty, pen_delimiter,
                                                                                       scaler, subset_filter,
                                                                                       test_years, train_years,
                                                                                       transformation)

        if not subset_using.empty:
            subset_using = subset_using + subset_filter.astype(int)
        else:
            subset_using = subset_filter.astype(int)

        results[get_mongo_key(filters)] = min_res
        # results_examples[get_mongo_key(filters)] = subset_filter.sum()
        fin_class.append((filters, min_cls))

    used = dict()

    export_classifier(fin_class, exp_name, exp_pbs_id, columns, scaler, experiment_info, data_subset)

    aggregated_results = create_aggregated_results(features, results, used)

    zero_used = sum(subset_using < 1)
    multiple_used = sum(subset_using > 1)

    print(aggregated_results)
    print(aggregated_results[err_fea])

    return {"results": results, "aggregated_results": aggregated_results, "zero_used": zero_used,
            "multiple_used": multiple_used, "example_count": used}


def find_optimal_classifier_for_filter(category_limit, classifiers, columns, err_fea, features, filters,
                                       manufacture_command_semi, original_penalty, pen_delimiter, scaler, subset_filter,
                                       test_years, train_years, transformation):
    subset_filter = None
    min_correct = -float("inf")
    min_res = None
    min_cls = None
    for construct_function, it in classifiers:
        classifier, lt_dic, _, test_data, x, subset_filter = make_experiment(category_limit, columns,
                                                                             construct_function, filters, it,
                                                                             manufacture_command_semi,
                                                                             original_penalty, scaler, test_years,
                                                                             train_years, transformation,
                                                                             pen_delimiter)

        features = lt_dic
        if lt_dic[err_fea][0] > min_correct:
            min_res = construct_function, it, lt_dic
            min_correct = lt_dic[err_fea][0]
            min_cls = classifier
    return features, min_cls, min_res, subset_filter


def create_aggregated_results(features, results, used):
    aggregated_results = dict()
    for fea in features:
        aggr = 0
        s = 0
        for fil in results.keys():
            r = results[fil][2][fea]
            aggr += r[0] * r[1]
            s += r[1]

        aggregated_results[fea] = aggr / s
        used[fea] = s
    return aggregated_results


def make_experiment(category_limit, columns, construct_function, filters, it, manufacture_command_semi,
                    original_penalty, scaler, test_years, train_years, transformation, pen_delimiter):
    """
    Train one classifier over dataframe (divide by filter, penalty divided by pen_delimiter)
    :param category_limit:
    :param columns: columns to train
    :param construct_function: function construct classifier
    :param filters:
    :param it: parameter for classifier
    :param manufacture_command_semi: file_name
    :param original_penalty:
    :param scaler: dataframe scaler
    :param test_years: list of years to test
    :param train_years: list of years to train
    :param transformation: describe of transformation of penalty
    :param pen_delimiter: delimiter penalty to category
    :return:
    """

    lt_dic = dict()
    classifier, x = construct_function(it)
    print(classifier)
    print("parameter:", x)
    data_subset, _, subset_filter = get_datasubset(manufacture_command_semi, train_years, test_years, original_penalty,
                                                   transformation,
                                                   filters)
    data_subset_scaled = data_subset
    data_subset_scaled.loc[:, columns] = scaler.transform(data_subset[columns])
    train_data, test_data, validation = split_dataset(data_subset_scaled, train_years)
    td = train_data[columns]
    tp = train_data[TRANS_PEN]
    cls = classifier.fit(td, tp)
    pen = cls.predict(train_data[columns])
    for func, lim in category_limit:
        lt_dic[str(lim).replace(".", DOT_REPLACE).replace(" ", "") + "_" + func.__name__ + "_train"] = \
            func(pen, train_data[TRANS_PEN], lim), len(pen)
    lt_dic["mse_train"] = mse_err(pen, train_data[TRANS_PEN]), len(pen)
    pen = cls.predict(test_data[columns])
    # print("train pen >1M", (train_data["_penalty"] > 1000000).sum(), len(train_data))
    # print("test pen >1M", (test_data["_penalty"] > 1000000).sum(), len(test_data))
    for func, lim in category_limit:
        lt_dic[str(lim).replace(".", DOT_REPLACE) + "_" + func.__name__] = func(pen, test_data[TRANS_PEN], lim), len(
            pen)
        for f in pen_delimiter:
            _, _, keys = filter_dataframe(test_data, f)
            lt_dic[str(lim).replace(".", DOT_REPLACE) + "_" + func.__name__ + filter_to_str(f, True).replace(".",
                                                                                                             DOT_REPLACE)] = func(
                pen[keys], test_data[keys][TRANS_PEN], lim), len(pen[keys])

    msev = mse_err(pen, test_data[TRANS_PEN]), len(pen)
    lt_dic["mse_test"] = msev

    print("lt_dic:", lt_dic)
    print("mse:", msev)
    return cls, lt_dic, msev, test_data, x, subset_filter


if __name__ == "__main__":
    examples_ = "categorical_percentage"
    train_years_ = [2014]
    test_years_ = [2015]
    columns_ = ['command_price', 'ongoing_pieces', 'start_month', 'nrow_TAC', 'sum_TAC', 'material_sum',
                'material_nrow', 'parent_pieces_order_count', 'parent_depth', 'parent_count',
                'ongoing_manufacture_command', "end_stock_price", "actual_cost"]
    cls_ = [(construct_tree, i) for i in range(4, 6)]
    conf_ = dict()

    conf_["category_limit"] = [(lt_percentage_err, (0.1, 50000)), (lt_percentage_err, (0.2, 50000)), (lt_err, 100000),
                               (lt_err, 250000), (lt_err, 1000000), (lt_log_err, (1, 10))]

    conf_["cls_dict"] = [([[("command_price", le, 1000), ("actual_cost", le, 1000)]], cls_),
                         ([[("command_price", gt, 1000), ("actual_cost", gt, 1000)]], cls_),
                         ([[("command_price", le, 1000), ("actual_cost", gt, 1000)]], cls_),
                         ([[("command_price", gt, 1000), ("actual_cost", le, 1000)]], cls_),
                         ]

    conf_["cls_dict"] = [([[]], cls_)]

    conf_["original_penalty"] = "cost_sin_2_6"
    conf_["train_years"] = train_years_
    conf_["test_years"] = test_years_
    conf_["columns"] = columns_
    conf_[
        "manufacture_command_semi"] = GENERAL_FOLDER_FOR_COMPANIES + "/" + COMPANY_NAME + "_" + "manufacture_command_ex.csv"
    scaler_ = StandardScaler()
    conf_["scaler"] = scaler_
    conf_["err_fea"] = '[0{_}1,50000]_lt_percentage_err_train'
    conf_["transformation"] = [('min_to_one', None), ("log", 10)]
    conf_["transformation"] = [('min_to_one', None)]
    conf_["transformation"] = []
    conf_["company"] = COMPANY_NAME
    conf_["name"] = "test_conf"
    conf_["pen_delimiter"] = [
        [[("_penalty", le, 250000)]],
        [[("_penalty", gt, 250000), ("_penalty", le, 1000000)]],
        [[("_penalty", gt, 1000000)]]
    ]

    if examples_ == "days":
        conf_["manufacture_command_semi"] = GENERAL_FOLDER_FOR_COMPANIES + "/" + COMPANY_NAME + "_" + "manufacture_command_ex.csv"
        conf_["original_penalty"] = "cost_sin_days_2_6"

    if examples_ == "categorical":
        conf_["category_limit"] = [(exact_acc, ())]
        conf_["transformation"] = [("to_categorical", [250000, 1000000])]
        conf_["err_fea"] = '[]_exact_acc'
        cls_ = [(standard_classifiers, it) for it in range(23)]
        conf_["cls_dict"] = [([[]], cls_)]

    if examples_ == "categorical_percentage":
        conf_["category_limit"] = [(exact_acc, ())]
        conf_["transformation"] = [("to_categorical_percentage", [33, 66])]
        conf_["err_fea"] = '[]_exact_acc'
        cls_ = [(standard_classifiers, it) for it in range(23)]
        conf_["cls_dict"] = [([[]], cls_)]

    conf_.update(json.loads(sys.argv[1]))
    conf_["exp_name"] = sys.argv[2]
    conf_["exp_pbs_id"] = sys.argv[3].split(".")[0]

    print(conf_)
    # print(conf_["cls_dict"][0][1][0][2])

    conf_["experiment_info"] = conf_.copy()

    run_ = ex.run(config_updates=conf_)

    print("rst", run_.result)
    agg_res = run_.result["aggregated_results"]
    agg_count = run_.result["example_count"]
    print("agg res:")
    for k, it in agg_res.items():
        print(k, "acc:", it, "samples:", agg_count[k])
