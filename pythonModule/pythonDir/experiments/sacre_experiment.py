import logging
import pickle

from sacred import Experiment
from sacred.observers import MongoObserver

from experiments.se_utils import make_experiment

ex = Experiment("asseco penalty estimation", interactive=True)

ex.observers.append(MongoObserver.create(
    url='mongodb://asseco_sacred:Mai0eiYeiZ@biodata.felk.cvut.cz:27001',
    db_name='asseco_sacred'))

ll = logging.getLogger("asd")
ll.setLevel(logging.WARNING)

ex.logger = ll


@ex.config
def config():
    category_limit = None
    company = None
    train_years = None
    test_years = None
    original_penalty = None
    penalty = None
    transformation = None
    filters = None
    construct_function = None
    classifier_name = None
    it = None
    category_limit = None
    columns = None
    name = None
    manufacture_command_semi = None
    random = None
    scaler = None
    text_info = """use filter to data
    scale data
    """


@ex.main
def run(construct_function, it, category_limit, original_penalty, columns, manufacture_command_semi, train_years,
        test_years, transformation, filters, scaler):
    classifier, lt_dic, msev, test_data, x, _ = make_experiment(category_limit, columns, construct_function, filters, it,
                                                                manufacture_command_semi, original_penalty, scaler,
                                                                test_years, train_years, transformation)

    return lt_dic, msev.tolist(), x, pickle.dumps(classifier), test_data.to_msgpack()
