"""
File generating PBS script for metacentrum.
"""

import argparse
import copy
import itertools
import math
import json as jjson
from typing import List, Tuple, Callable

import jsonpickle as json
import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler

from config import COMPANY_NAME, GENERAL_FOLDER_FOR_COMPANIES
from error_functions import exact_acc
from experiments.own_classifier import construct_tree, construct_random_forest, standard_classifiers, VERSION
from utils import le, gt, get_datasubset


def generate(inp: List[Tuple[str, str, int, int]], dataset: pd.DataFrame, estimators: List) \
        -> List[List[Tuple[List, List]]]:
    """
    From inp (described partitioning of space) create list of list of filters where is all possible partitioning of
    space described with filters.
    :param dataset:
    :param estimators:
    :param inp: list of quartets - feature, partitioning, bins, order
        partitioning - "uniform", "log"
        order = {-1,1} - important for log: (0-64) 1 => 2,4,8,16,32; -1 => 62,60,56,48,32
    :return:
    """
    inp_len = len(inp)
    out = _rec(inp, dataset)
    out2 = [list(filter(lambda x: x, [minimize_filter(sub_space) for sub_space in cls])) for cls in out]
    out2 = [[[it] for it in iit] for iit in out2]
    if not inp_len:
        out2 = [[out2]]
    out3 = [[(it, estimators) for it in iit] for iit in out2]
    return out3


def _apply_bound(fea, x, rules):
    out = []
    for ru in rules:
        rr = ru.copy()
        rr.append((fea, le, x))
        out.append(rr)
        rr = ru.copy()
        rr.append((fea, gt, x))
        out.append(rr)
    return out


def minimize_filter(fil: List[Tuple[str, Callable, float]]):
    """
    Remove useless part of filter. Now accept only gt and le operator.
    :param fil: filter to minimize
    :return: minimized filter
    """
    features = list(set(map(lambda x: x[0], fil)))
    out = []
    for fea in features:
        mmin = -float("inf")
        mmax = float("inf")
        for _, f, val in filter(lambda x: x[0] == fea, fil):
            if f == le and mmax > val:
                mmax = val
            if f == gt and mmin < val:
                mmin = val

        if mmax > mmin:
            if mmin != -float("inf"):
                out.append((fea, gt, mmin))
            if mmax != float("inf"):
                out.append((fea, le, mmax))
        else:
            return None
    return out


def _rec(inp: List[Tuple[str, str, int, int]], dataset: pd.DataFrame) -> List[List]:
    """Private function, called in function generate
    """
    if not inp:
        return [[[]]]
    feature, partitioning, bins, order = inp.pop()
    prev = _rec(inp, dataset)
    out = []
    bounds = [None]
    mmin = dataset[feature].min()
    mmax = dataset[feature].max()
    # print("mmin mmax", mmin, mmax)
    s = (mmax - mmin)

    if partitioning == "uniform":
        s = s / bins
        bounds = [mmin + s * i for i in range(1, bins)]

    if partitioning == "count":
        s = s / bins
        sorted_feature = np.sort(dataset[feature])
        off = len(sorted_feature) // bins
        bounds = [sorted_feature[off * i] for i in range(1, bins)]

    if partitioning == "log":
        s = math.pow(s, 1 / bins)
        start = mmin
        if order == -1:
            start = mmax
        bounds = [start + order * pow(s, i) for i in range(1, bins)]

    # bounds = list(zip([mmin] + bounds, bounds + [mmax]))
    # print(bounds)

    for cls in prev:
        for bo in bounds:
            out.append(_apply_bound(feature, float(bo), cls))

    return out


if __name__ == "__main__":
    parser = argparse.ArgumentParser("Generate description files for training classifiers.")
    parser.add_argument('--number', '-n', type=int, default=0,
                        help='Number of classifier')
    parser.add_argument('--columns', '-cl', nargs='+', type=str, default=None,
                        help='List of column consider as feature.')
    parser.add_argument('--penalty', '-p', type=str, help='Penalty column to predict.', default='cost_sin_2_6')
    parser.add_argument('--company', '-c', type=str,
                        help='Company string identifier (same as folder with compandy data), now it is possible '
                             'choices hudba, pribory, letadla. ')
    parser.add_argument('--start_years_train', '-str', type=int, help='Start of training range.')
    parser.add_argument('--start_years_test', '-ste', type=int, help='Start of testing range.')
    parser.add_argument('--end_years_train', '-etr', type=int, help='End of training range.')
    parser.add_argument('--end_years_test', '-ete', type=int, help='End of training range.')
    parser.add_argument('--pbs_job_name', '-pjn', type=str, default="$PBS_JOBNAME",
                        help='Job name, for run on metacenter use default value.')
    parser.add_argument('--pbs_jobid', '-pjd', type=str, default="$PBS_JOBID",
                        help='Job id, use only with generating only one classifier script, for run on metacenter use '
                             'default value.')
    parser.add_argument('-uniform', action='store_true',
                        help='Use flag for generating classifiers description file '
                             'with uniform filter partitioning.')
    parser.add_argument('-log', action='store_true',
                        help='Use flag for generating classifiers description file with '
                             'uniform filter partitioning.')
    parser.add_argument('-count', action='store_true', help='Use flag for generating classifiers description file '
                                                            'with count filter partitioning.')


    params = parser.parse_args()
    if not params.columns:
        columns = ['command_price', 'start_month', 'nrow_TAC', 'sum_TAC', 'material_sum', 'material_nrow',
                   'parent_pieces_order_count', "parent_pieces_order_mean", "parent_pieces_order_max",
                   "parent_pieces_order_min", 'parent_depth', 'parent_count', "end_stock_price", "actual_cost",
                   "ongoing_same_pieces", "ongoing_same_command", "ongoing_all_command", "ongoing_all_pieces"]
    else:
        columns = params.columns
    quartets = []
    if params.uniform:
        quartets.extend([(fea, "uniform", bins, 0) for fea in columns for bins in [3, 4]])
    if params.log:
        quartets.extend([(fea, "count", bins, 0) for fea in columns for bins in [3, 4]])
    if params.count:
        quartets.extend([(fea, "log", bins, order) for fea in columns for bins in [3, 4] for order in [-1, 1]])
    # print(len(quartets))
    # job_quartets = list(itertools.combinations(quartets, 3))
    # job_quartets = list(itertools.combinations(quartets, 0))
    job_quartets = [[]]
    job_quartets.extend(list(itertools.combinations(quartets, 1)))
    # job_quartets = list(itertools.combinations(quartets, 1))
    job_quartets = list(map(list, job_quartets))

    job_quartets = copy.deepcopy(job_quartets)

    job = []

    ll = len(job_quartets)
    # print(ll)

    cls_ = [(construct_tree, i) for i in range(4, 20)]
    cls_.extend([(construct_random_forest, (i,)) for i in range(3, 20)])

    # print(len(job))

    gen = "gen"
    conf_ = dict()

    train_years_ = set(range(params.start_years_train, params.end_years_train))
    test_years_ = set(range(params.start_years_test, params.end_years_test))
    conf_["train_years"] = list(train_years_ - set(test_years_))
    test_years_ = list(test_years_)
    train_years_ = list(train_years_)
    COMPANY_NAME = params.company
    gen = gen + str(test_years_)
    percentage = [33, 66]

    myval = params.number
    cls_ = [(standard_classifiers, it) for it in range(myval, myval + 1)]

    conf_["category_limit"] = [(exact_acc, ())]

    conf_["cls_dict"] = [([[("command_price", le, 1000), ("actual_cost", le, 1000)]], cls_),
                         ([[("command_price", gt, 1000), ("actual_cost", gt, 1000)]], cls_),
                         ([[("command_price", le, 1000), ("actual_cost", gt, 1000)]], cls_),
                         ([[("command_price", gt, 1000), ("actual_cost", le, 1000)]], cls_),
                         ]

    conf_["original_penalty"] = params.penalty
    conf_["test_years"] = test_years_
    conf_["columns"] = columns
    conf_["manufacture_command_semi"] = GENERAL_FOLDER_FOR_COMPANIES + "/" + COMPANY_NAME + ".csv"
    scaler_ = StandardScaler()
    conf_["scaler"] = scaler_
    conf_["name"] = gen + "_" + str(percentage)
    conf_["pen_delimiter"] = [
        [[("_penalty", le, 250000)]],
        [[("_penalty", gt, 250000), ("_penalty", le, 1000000)]],
        [[("_penalty", gt, 1000000)]]
    ]

    conf_["relevant"] = False
    conf_["transformation"] = [("to_categorical_percentage", percentage)]
    conf_["err_fea"] = '[]_exact_acc'
    conf_["company"] = COMPANY_NAME
    conf_["text_info"] = "correct tst and trn year for own cls partitioning"

    # print(json.dumps(conf_).replace('"', '\\"'))
    json.loads(json.dumps(conf_))

    df = get_datasubset(GENERAL_FOLDER_FOR_COMPANIES + "/" + COMPANY_NAME + ".csv", conf_["train_years"], conf_["test_years"],
                        conf_["original_penalty"], [], [[]])[0]

    for i, it in list(enumerate(list(job_quartets))):
        it = list(it)
        # if not i % int(math.sqrt(ll)):
        #     print(i, len(job))
        job.extend(generate(it, df, cls_))

    for i, it in enumerate(job):
        # print("cls_dict:", it)
        conf_["cls_dict"] = it
        out = """ "{}" """.format(
            json.dumps(conf_).replace('"', '\\"'))
        print(json.dumps(conf_).replace('"', '\\"'))

        f_ = open("gen_cls/" + COMPANY_NAME + "_cls_" + gen + "_" + str(i) + "_" + str(myval) + ".json", "w",
                  encoding="utf8",
                  newline='\n',)
        f_.write(out)
        f_.close()

        # with open("myGen_cls" + "_" + str(myval) + '.json', 'w') as fp:
        #     jjson.dump(conf_, fp)
