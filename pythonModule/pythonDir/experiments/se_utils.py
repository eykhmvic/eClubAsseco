from utils import split_dataset, mse_err, get_datasubset, TRANS_PEN, filter_dataframe, DOT_REPLACE, filter_to_str


def make_experiment(category_limit, columns, construct_function, filters, it, manufacture_command_semi,
                    original_penalty, scaler, test_years, train_years, transformation, pen_delimiter):

    lt_dic = dict()
    classifier, x = construct_function(it)
    print(classifier)
    print("parameter:", x)
    data_subset, _, subset_filter = get_datasubset(manufacture_command_semi, train_years, test_years, original_penalty,
                                                   transformation,
                                                   filters)

    data_subset_scaled = data_subset
    data_subset_scaled.loc[:, columns] = scaler.transform(data_subset[columns])
    train_data, test_data, validation = split_dataset(data_subset_scaled, train_years)
    td = train_data[columns]
    tp = train_data[TRANS_PEN]
    cls = classifier.fit(td, tp)
    pen = cls.predict(train_data[columns])
    for func, lim in category_limit:
        lt_dic[str(lim).replace(".", DOT_REPLACE).replace(" ", "") + "_" + func.__name__ + "_train"] = \
            func(pen, train_data[TRANS_PEN], lim), len(pen)
    lt_dic["mse_train"] = mse_err(pen, train_data[TRANS_PEN]), len(pen)
    pen = cls.predict(test_data[columns])
    # print("train pen >1M", (train_data["_penalty"] > 1000000).sum(), len(train_data))
    # print("test pen >1M", (test_data["_penalty"] > 1000000).sum(), len(test_data))
    for func, lim in category_limit:
        lt_dic[str(lim).replace(".", DOT_REPLACE) + "_" + func.__name__] = func(pen, test_data[TRANS_PEN], lim), len(
            pen)
        for f in pen_delimiter:
            _, _, keys = filter_dataframe(test_data, f)
            lt_dic[str(lim).replace(".", DOT_REPLACE) + "_" + func.__name__ + filter_to_str(f, True).replace(".", DOT_REPLACE)] = func(
                pen[keys], test_data[keys][TRANS_PEN], lim), len(pen[keys])

    msev = mse_err(pen, test_data[TRANS_PEN]), len(pen)
    lt_dic["mse_test"] = msev

    print("lt_dic:", lt_dic)
    print("mse:", msev)
    return classifier, lt_dic, msev, test_data, x, subset_filter
