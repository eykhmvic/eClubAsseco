from use_classifier import main
from os import sep as my_separator
import argparse


def run(cl):
    main(
        'pythonModule' + my_separator +
        'pythonDir' + my_separator +
        'classifiers' + my_separator +
        cl)


if __name__ == '__main__':
    parser = argparse.ArgumentParser("Get classifier.")
    parser.add_argument('--classifier', '-classifier', type=str, default=None,
                        help='name of classifier')
    params = parser.parse_args()
    cl = params.classifier
    run(cl)
