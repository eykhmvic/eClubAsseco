import argparse
import heapq
import sys

from pymongo import MongoClient


def main(company, db, version, best_result, mongo_url):
    if not mongo_url:
        mongo_url = 'mongodb://asseco_sacred:Mai0eiYeiZ@biodata.felk.cvut.cz:27001'
    client = MongoClient(mongo_url)
    db = client[db]
    query = {"config.company": company, "result": {"$ne": "null"}}
    if version:
        query["config.version"] = version

    runs = db.runs

    l = []

    for run in runs.find(query):
        if run["result"]:
            res = run["result"]["aggregated_results"]
            rel_res = list(filter(lambda it: "[]_exact_acc[" in it[0], res.items()))
            prod = 1
            for it in rel_res:
                prod *= it[1]
            heapq.heappush(l, (-prod, run["_id"], run))

    for i in range(best_result):
        it = heapq.heappop(l)
        it_cnf = it[2]["config"]
        name = it_cnf["exp_name"] + ".cls" + it_cnf["exp_pbs_id"]
        res = "Score: {:5f}, score geometric mean {:5f}, classifier name: {}".format(
            it[2]["result"]["aggregated_results"]["[]_exact_acc"], -it[0], name)
        print(res)


if __name__ == "__main__":
    parser_ = argparse.ArgumentParser(
        description='Return best (geometric mean) classifier for company in database.')
    parser_.add_argument('--company', '-c', required=True, type=str,
                         help='identifiers of company')
    parser_.add_argument('--database', '-db', default='asseco_sacred', type=str,
                         help='name of database')
    parser_.add_argument('--classifier_version', '-cv', type=int,
                         help='version of code of classifier')
    parser_.add_argument('--best_result', '-br', type=int, default=3,
                         help='Number of returned classifiers.')
    parser_.add_argument('--db_url', type=str,
                         help='URL to connect to database like mongodb://auth_db:password@server:port')
    params_ = parser_.parse_args(sys.argv[1:])

    main(params_.company, params_.database, params_.classifier_version, params_.best_result, params_.db_url)
