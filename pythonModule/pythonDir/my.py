import argparse
import heapq
import sys

from pymongo import MongoClient


def main(db, version, mongo_url):
    if not mongo_url:
        mongo_url = 'mongodb://asseco_sacred:Mai0eiYeiZ@biodata.felk.cvut.cz:27001'
    client = MongoClient(mongo_url)
    db = client[db]
    # "config.name": {"$regex": "testtest"},
    query = { "config.exp_name": "aa"}
    if version:
        query["config.version"] = version

    print(query)


    runs = db.runs

    l = []

    print("before cycle")
    for run in runs.find(query):
        print(run)
        if run["result"]:
            print('----------------------------\n', run['config']['exp_pbs_id'], "\n",
                  "\n".join(map(str, run['result']['aggregated_results'].items())))


if __name__ == "__main__":
    parser_ = argparse.ArgumentParser(
        description='Return best (geometric mean) classifier for company in database.')
    # parser_.add_argument('--company', '-c', required=True, type=str,
    #                      help='identifiers of company')
    parser_.add_argument('--database', '-db', default='asseco_sacred', type=str,
                         help='name of database')
    parser_.add_argument('--classifier_version', '-cv', type=int,
                         help='version of code of classifier')
    parser_.add_argument('--best_result', '-br', type=int, default=3,
                         help='Number of returned classifiers.')
    parser_.add_argument('--db_url', type=str,
                         help='URL to connect to database like mongodb://auth_db:password@server:port')
    params_ = parser_.parse_args(sys.argv[1:])

    main(params_.database, params_.classifier_version, params_.db_url)
