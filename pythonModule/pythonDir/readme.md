jupyter notebook --NotebookApp.iopub_data_rate_limit=10000000000

# Popis příznaků
Uvádím jednotlivé příznaky a jejich krátký popis
'manufacture_cost', 'nrow_TAC', 'mean_TAC', 'sum_TAC', 'mean_dobaZakazka', 'mean_dobaKus',
                'mean_kusy_odved', 'integrated_pieces', 'integrated_cost', 'count_of_order', 'mean_stock_load_2014',
                'mean_stock_cost_2014', 'median_stock_load_2014', 'var_stock_load_2014', 'var_stock_cost_2014',
                'mean_stock_load_2015', 'mean_stock_cost_2015', 'median_stock_load_2015', 'var_stock_load_2015',
                'var_stock_cost_2015'


# Nahrávání data

Pro nahrávání dat z databáze slouží SqlConnect.
Ve kterém je několik předpřipravených sql dotazů.

## Zakázky


# Zpracování dat
## Polotovary

* count_of_order - počet zakázek pro polotovar


# Data

## SQL
Popis stahovaných dat z SQL, za "=>" jsou přibližné sloupečky které jsou počítány na základě dotazu

Potřebné parametry pro zakázky => ongoing_task, ongoing_count 
```SQL
SELECT
    VPrikazKmenZbozi.RegCis AS id,
    TabPrikaz.ID            AS order_id,
    TabPrikaz.Rada,
    TabPrikaz.StavPrikazu,
    TabPrikaz.IDTabKmen,
    TabPrikaz.IDZakazka,
    TabPrikaz.kusy_ciste,
    TabPrikaz.kusy_zad,
    VPrikazKmenZbozi.Nazev1,
    VPrikazZakazka.CisloZakazky,
    tabPrikaz.plan_zadani,
    tabPrikaz.plan_ukonceni,
    tabPrikaz.zadani_X,
    tabPrikaz.ukonceni_X,
    tabPrikaz.kusy_odved,
    tabPrikaz.kusy_zmet
FROM TabPrikaz
  LEFT OUTER JOIN TabKmenZbozi VPrikazKmenZbozi ON VPrikazKmenZbozi.ID = TabPrikaz.IDTabKmen
  LEFT OUTER JOIN TabZakazModifDilce VPrikazZakazModifDilce
    ON VPrikazZakazModifDilce.IDZakazModif = TabPrikaz.IDZakazModif AND
       VPrikazZakazModifDilce.IDKmenZbozi = TabPrikaz.IDTabKmen
  LEFT OUTER JOIN TabZakazka VPrikazZakazka ON VPrikazZakazka.ID = TabPrikaz.IDZakazka
```

Zdroj pro pohyby na skladu => tabulka polotovar x stav skladu po dnech, actual_pieces, actual_cost, end_stock_price
```SQL
SELECT
    KMEN.RegCis                             AS id,
    KMEN.Nazev1                             AS product_name,
    KMEN.SkupZbo                            AS store_group,
    POHYBY.druhPohybuZbo                    AS load_unload,
    POHYBY.mnozstvi                         AS amount,
    POHYBY.mnozstviDruhove                  AS sign_amount,
    POHYBY.skutecneDatReal_X                AS date,
    POHYBY.skutecneDatReal_Y                AS year,
    POHYBY.skutecneDatReal_M                AS month,
    POHYBY.skutecneDatReal_D                AS day,
    POHYBY.CisloZakazky AS order_id
    
    FROM TabStavSkladu SKLAD JOIN TabKmenZbozi KMEN ON SKLAD.IDkmenZbozi=KMEN.ID
    INNER JOIN TabPohybyZbozi AS POHYBY ON POHYBY.IDZboSklad = SKLAD.ID
    WHERE POHYBY.DruhPohybuZbo >= 0 AND POHYBY.DruhPohybuZbo <= 4 AND
        (KMEN.SkupZbo >= 200 AND KMEN.SkupZbo < 300)
                   AND DruhPohybuZbo >= 0 AND DruhPohybuZbo <= 4
```

Parametry potřebné pro výrobní příkazy => (vše co je přenásobené počtem kusů ve výrobním příkazu (command_price(end_stock_price), time_complexity, )), (vše co je k zadání, nebo ukončení výrobního příkazu (ongoing_pieces, ongoing_task, actual_cost)), start_month
```SQL
SELECT
----TabPrikaz.ID ,TabPrikaz.BlokovaniEditoru,TabPrikaz.Rada,TabPrikaz.StavPrikazu,TabPrikaz.IDTabKmen,TabPrikaz.IDZakazModif,TabPrikaz.IDZakazka,TabPrikaz.kusy_ciste,TabPrikaz.kusy_zad,TabPrikaz.kusy_zive,TabPrikaz.EvidJednotka,TabPrikaz.IDRezervace,TabPrikaz.IDPlan,TabPrikaz.IDPrikazVyssi,TabPrikaz.IDPrikazRidici,TabPrikaz.IDObchPrip,TabPrikaz.IDNaklOkruh,tabPrikaz.prikaz,VPrikazKmenZbozi.SkupZbo,VPrikazKmenZbozi.RegCis,VPrikazKmenZbozi.Nazev1,(SELECT ZM.Kod FROM TabZakazModif ZM WHERE ZM.ID=VPrikazZakazModifDilce.IDZakazModif) AS KodModifikace,VPrikazZakazka.CisloZakazky,tabPrikaz.plan_zadani,tabPrikaz.plan_ukonceni,tabPrikaz.zadani_X,tabPrikaz.ukonceni_X,tabPrikaz.kusy_odved,tabPrikaz.kusy_zmet,
TabPrikaz.ID AS idd,
TabPrikaz.kusy_ciste AS amount_clear,
TabPrikaz.kusy_zad AS amount,
VPrikazKmenZbozi.SkupZbo AS store_group,
VPrikazKmenZbozi.RegCis AS product_id,
VPrikazKmenZbozi.Nazev1 AS product_name,
VPrikazZakazka.CisloZakazky order_id,
tabPrikaz.plan_zadani AS planed_start,
tabPrikaz.plan_ukonceni AS planed_end,
tabPrikaz.zadani_X AS start_date,
tabPrikaz.ukonceni_X AS end_date,
tabPrikaz.kusy_odved AS amount_produced,
tabPrikaz.kusy_zmet AS amount_rejected
FROM TabPrikaz
LEFT OUTER JOIN TabKmenZbozi VPrikazKmenZbozi ON VPrikazKmenZbozi.ID=TabPrikaz.IDTabKmen
LEFT OUTER JOIN TabZakazModifDilce VPrikazZakazModifDilce ON VPrikazZakazModifDilce.IDZakazModif=TabPrikaz.IDZakazModif AND VPrikazZakazModifDilce.IDKmenZbozi=TabPrikaz.IDTabKmen
LEFT OUTER JOIN TabZakazka VPrikazZakazka ON VPrikazZakazka.ID=TabPrikaz.IDZakazka
```

Zdroj pro strojní čas => sumTAC, nrowTAC, meanTAC
```SQL
SELECT
VPostupKmenZbozi.Nazev1 AS nazev1,
tabPostup.nazev,
tabPostup.tac_N AS TAC,
VPostupKmenZbozi.RegCis AS id
FROM TabPostup
  LEFT OUTER JOIN TabCzmeny VPostupCZmenyOd ON TabPostup.ZmenaOd=VPostupCZmenyOd.ID
  LEFT OUTER JOIN TabCzmeny VPostupCZmenyDo ON TabPostup.ZmenaDo=VPostupCZmenyDo.ID
  LEFT OUTER JOIN TabKmenZbozi VPostupKmenZbozi ON TabPostup.dilec=VPostupKmenZbozi.ID
  LEFT OUTER JOIN TabCPraco VPostupCPraco ON TabPostup.pracoviste=VPostupCPraco.ID
WHERE
(((TabPostup.IDZakazModif IS NOT NULL OR  VPostupCZmenyOd.platnostTPV=1 AND VPostupCZmenyOd.datum<=GETDATE() AND  (VPostupCZmenyDo.platnostTPV=0 OR TabPostup.ZmenaDo IS NULL OR (VPostupCZmenyDo.platnostTPV=1 AND VPostupCZmenyDo.datum>GETDATE())) )))
```

Zdroj pro material => material_nrow, material_SUM, parent_count, parent_depth
```SQL
SELECT
TabKvazby.ID,
VKVazbyVyssiKmenZbozi.RegCis AS id,
VKVazbyVyssiKmenZbozi.nazev1 AS name,
VKVazbyNizsiKmenZbozi.material,
VKVazbyNizsiKmenZbozi.RegCis AS id_down,
VKVazbyNizsiKmenZbozi.nazev1 AS name_down,
TabKvazby.mnozstvi AS amount
FROM TabKvazby
  LEFT OUTER JOIN TabCzmeny VKVazbyZmenaOdCZmeny ON TabKvazby.ZmenaOd=VKVazbyZmenaOdCZmeny.ID
  LEFT OUTER JOIN TabCzmeny VKVazbyZmenaDoCZmeny ON TabKvazby.ZmenaDo=VKVazbyZmenaDoCZmeny.ID
  LEFT OUTER JOIN TabKmenZbozi VKVazbyVyssiKmenZbozi ON VKVazbyVyssiKmenZbozi.ID=TabKvazby.vyssi
  LEFT OUTER JOIN TabZakazModif VKVazbyZakazModif ON VKVazbyZakazModif.ID=TabKvazby.IDZakazModif
  LEFT OUTER JOIN TabCVariant VKVazbyVyssiCVariant ON VKVazbyVyssiCVariant.ID=TabKvazby.IDVarianta
  LEFT OUTER JOIN TabKmenZbozi VKVazbyNizsiKmenZbozi ON TabKvazby.nizsi=VKVazbyNizsiKmenZbozi.ID
WHERE
(((TabKvazby.IDZakazModif IS NOT NULL OR VKVazbyZmenaOdCZmeny.platnostTPV=1 AND VKVazbyZmenaOdCZmeny.datum<=GETDATE() AND (VKVazbyZmenaDoCZmeny.platnostTPV=0 OR TabKvazby.ZmenaDo IS NULL OR (VKVazbyZmenaDoCZmeny.platnostTPV=1 AND VKVazbyZmenaDoCZmeny.datum>GETDATE())) )))
```


## data_cont
dataframe obsahující vývoj na skladě

## data_cont_en



## order_cont
*

## manufacture_command
Obsahuje data pro jednotlivé výrobní příkazy. Pro klasifikaci se aktualne pouzivaji tyto sloupce ['command_price', 'ongoing_pieces', 'start_month', 'nrow_TAC', 'sum_TAC', 'material_sum', 'material_nrow', 'parent_pieces_order_count', 'parent_depth', 'parent_count', ongoing_manufacture_command', "end_stock_price", "actual_cost"]
* idd - id výrobního příkazu (TabPrikaz.ID)
* amount - množství požadované na výrobním příkazu (TabPrikaz.kusy_zad)
* amount_clear - kusy čisté (TabPrikaz.kusy_ciste)
* actual_pieces - akt. stav na sklade [ks]
* actual_cost - akt. stav na sklade [$$$]
* end_stock_price  - součet aktuální ceny na skladě a hodnoty výrobního příkazu (celková hodnota skladu po ukončení výrobního příkazu)
* time_complexity - zatizeni linky pozad. kusy (počet polotovarů ve výrobním příkazu * TAC polotovaru)
* command_price - cena na sklade po vyrobeni (počet polotovarů ve výrobním příkazu * cena polotovaru)
* ongoing_all_command - počet běžících výrobních příkazů v den zadání výrobního příkazu
* ongoing_all_pieces - suma kusů z ongoing_all_command
* ongoing_same_command - počet běžících výrobních příkazů v den zadání výrobního příkazu se stejným produktem
* ongoing_same_pieces - suma produktů na výrobních příkazech z ongoing_same_command [ks]
* parent_count -- počet unikátních rodičůve stromu polotovarů
* parent_depth -- maximální hloubka rodičů ve stromu polotovarů
* parent_pieces_order_count -- počet kusů rodičů ve výrobních příkazech
* start_month -- měsíc kdy byl výrobní příkaz zadán
* command_price -- počet kusů na výrobním příkazu * cena polotovaru
* material_nrow -- počet materiálů vstupujících do výroby polotovaru
* material_sum -- součet materiálů vstupujících do výroby polotovaru
* sumTAC -- součet strojních časů potřebných pro výrobu polotovaru
* nrowTAC -- počet strojních operací potřebných pro výrobu polotovaru
* meanTAC -- průměr strojních časů potřebných pro výrobu polotovaru
* <strike>penalty - penalta dle Navrh predikce [170608] -  (60 dní, stav skladu vyšší jak 20k, od dokončení výrobního příkazu)</strike>

| Nazev        | DB nazev           | Popis  |
|:------------- |:--------------|:-----|
| idd           | TabPrikaz.ID |  id výrobního příkazu |
| amount        | TabPrikaz.kusy_zad      |  množství požadované na výrobním příkazu |
| amount_clear  | TabPrikaz.kusy_ciste      |    kusy čisté |
| actual_pieces | -- | Aktuální počet polotovarů kusů na skladě v den zadání výrobního příkazu |
| actual_cost   | -- | Cena kusů polotovarů na skladu v den zadání výrobního příkazu |
| command_price | -- | amout * 
| end_stock_price | -- | součet actual_cost a command_price |

### Penalty
Penalty je nyní počítána podle třech hlavních parametrů:
* počet dní
* váha jednotlivých dní
* transformační funkce

Váha a transformační funkce jsou popsány v config.py / WEIGHT_DICT, tento slovník obsahuje vždy název výsledné penalty a váhu a transformační funkci.
Pro výpočet penalty podle daných parametrů je funkce utils.py/calculate_penalty, která očekává řádek ve kterém je den po dni uvedeno množství polotovaru na skladě (délka podle počtu dní a prvním dnem je den ukončení výrobního příkazu).
Další je pandas serie kde jsou všechny výše uvedené příznaky (z manufacture_command), poté jsou váhy jednotlivých dní a transformační funkce z WEIGHT_DICT.

## manufacture_command_semi
Obsahuje data z manufacture_command, ale pouze pro polotovary.

## Transformace dat
Popis jednotlivých transformací prováděných na jednom sloupci hodnot:
* ('max', hodnota) - všechyn částky větší než zadaná hodnota nahradí za danou hodnotu
* ('min_to_zero', None) - od sloupečku odečte jeho minimum
* ('min_to_one', None) - od sloupečku odečtě jeho minimum a přičte k němu 1
* ('log', base) - celý sloupeček zlogaritmuje se základem base
* (‘scale_to_one’, None) - přeškáluje sloupeček na interval (0, 1)

# Conda enviroment preparation
conda create -n py35 python=3.5
activate py35
conda install jupyter h2o-py h2o pandas scikit-learn nb_conda matplotlib

# Výstupy dat

## duplicate semiproduct parent
(viz. duplicate_semiproduct_parent.json)
Obsahuje výpis polotovarů které mají duplicitu v rodičích (jeden z rodičů se vyskytuje vícekrát).
Výpis je v json formátu:
```javascript
[
    1,                  // počet duplicitních rodičů (len(arr) - len(set(arr)))
    [
       "0610856044",    // id polotovaru
       3,               // maximální hloubka stromu
      [                 // pole id rodičů
        "0610856044",
        "0610856040",
        "0610856210",
        "0610876040",
        "1910856444",
        "1910856440",
        "1910876440",
        "1910876440"
      ]
    ]
  ]
```

# Webové rozhraní
Dokumentace [zde](https://gitlab.com/wailord/eclubasseco/blob/master/web/readme.md).

