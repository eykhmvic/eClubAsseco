# Build: docker build --tag eclub_asseco .
# Run prerequisity: Data in the /data directories. See Readme.md file
# Run: docker run --interactive --tty --rm --name eclub_asseco \
#          --publish 8080:8080 \
#          --volume /data/manufacture_command_ex:/eclubAsseco/pythonModule/pythonDir/manufacture_command_ex \
#          --volume /data/data_cont:/eclubAsseco/pythonModule/pythonDir/data_cont eclub_asseco
# TODO: Do not run under root
# FIXME: Check for default installation paths of `python3` executable (so it is not needed to provide it via PYTHON_EXE env var)
FROM debian:stretch
# Exposed port (-p 8080:8080)
ENV PORT 8080
# Target directory of the project (inside container)
ENV TDIR /eclubAsseco
# Python 3 executable path (FIXME: Should not be used)
ENV PYTHON_EXE /usr/bin/python3
# Python modules path
ENV PYTHONPATH ${TDIR}/pythonModule/pythonDir
# Install python, JDK, maven
RUN apt-get update && apt-get install --yes \
        python3-pip \
    python3-tk \
        openjdk-8-jdk \
        maven && \
    rm -rf /var/lib/apt/*
# TODO: Move to pipenv/requirements.txt
# Install python dependencies
RUN pip3 install \
        scipy \
        graphviz \
        jupyter \
        matplotlib \
        numpy \
        pandas \
        pymongo \
        sacred \
        scikit-learn
# Populate project dir
RUN mkdir $TDIR
COPY pom.xml $TDIR/
COPY pythonModule $TDIR/pythonModule/
COPY src $TDIR/src
EXPOSE $PORT
# Volumes for data (CSV files)
VOLUME $TDIR/pythonModule/pythonDir/manufacture_command_ex
VOLUME $TDIR/pythonModule/pythonDir/data_cont
WORKDIR ${TDIR}
# Saves time on image rebuild
RUN mvn clean
RUN mvn compile
RUN mvn install
# For debugging purposes, we should use entrypoint.sh script (able to execute /bin/bash or any other cmd specified after docker run ...)
# This would also allow us to immediately exit the app (for downloading mvn dependencies on spring-boot: run)
ENTRYPOINT mvn spring-boot:run